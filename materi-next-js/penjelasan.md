<h1>01-Penjelasan-Materi-NextJS</h1>

Gitlab Repository : https://gitlab.com/sekjen-dpr/workshop/materi-next-js/ didalam folder /Materi

# React Js

## Penjelasan

React.js atau biasa disebut React adalah sebuah library open-source JavaScript yang dikembangkan oleh Facebook untuk membangun antarmuka pengguna (UI) pada aplikasi web. React memungkinkan pengembang web untuk membuat tampilan aplikasi web yang interaktif dan mudah dikelola dengan memanfaatkan komponen-komponen UI yang dapat digunakan kembali.

React menggunakan pendekatan pemrograman berorientasi komponen, di mana setiap komponen dianggap sebagai unit yang terpisah dan dapat digunakan kembali dalam berbagai bagian aplikasi. Komponen-komponen ini kemudian digunakan untuk membangun tampilan aplikasi web yang kompleks, namun tetap mudah dipahami dan dikelola.

React juga memiliki fitur-fitur seperti virtual DOM (Document Object Model) yang memungkinkan pengembang web untuk membuat perubahan pada tampilan aplikasi web secara efisien dan tanpa perlu melakukan refresh pada halaman. Selain itu, React juga mudah diintegrasikan dengan library-state management seperti Redux dan MobX, yang memungkinkan pengembang web untuk mengelola state aplikasi dengan lebih efektif.

React sangat populer di kalangan pengembang web karena mudah dipelajari dan digunakan, serta memiliki dukungan dan komunitas yang besar. React juga sering digunakan bersama dengan berbagai kerangka kerja (framework) dan teknologi modern lainnya untuk membangun aplikasi web yang lebih kompleks dan efisien.

## Referensi

- <https://reactjs.org/>

# Next Js

## Penjelasan

Next.js adalah sebuah framework yang dibangun di atas React.js. Next.js menyediakan fitur-fitur tambahan seperti **Server-Side Rendering (SSR)**, **Static Site Generation (SSG)**, automatic code splitting, dan routing yang sangat fleksibel. Dengan Next.js, pengembang web dapat membangun aplikasi web yang lebih cepat dan lebih mudah dengan memanfaatkan fitur-fitur modern yang disediakan oleh framework ini.

Next.js memperkenalkan konsep **Server-Side Rendering (SSR)** dan **Static Site Generation (SSG)**, yang memungkinkan pengembang untuk menghasilkan halaman HTML yang di-generate dari server sebelum ditampilkan ke browser. Hal ini dapat membantu meningkatkan kecepatan loading aplikasi dan memungkinkan aplikasi web dapat di-crawl oleh mesin pencari.

Selain itu, Next.js juga memudahkan pengembang untuk melakukan implementasi fitur-fitur seperti routing, pre-fetching data, optimasi gambar, dan masih banyak lagi. Next.js juga menyediakan dukungan untuk integrasi dengan berbagai jenis server seperti Express, Fastify, dan Koa.

Next.js sangat populer di kalangan pengembang React karena kemudahan penggunaannya dan dukungan yang kuat dari komunitas. Next.js banyak digunakan dalam pembuatan website, e-commerce, aplikasi dashboard, dan aplikasi web lainnya.

## Perbedaan antara Server-Side Rendering (SSR) & Client-Side Rendering (CSR)

Perbedaan utama antara Server-Side Rendering (SSR) dan Client-Side Rendering (CSR) pada React JS adalah bagaimana proses render dan pengiriman tampilan halaman pada aplikasi web.

Pada CSR, tampilan dibangun di sisi klien (browser) dengan menggunakan JavaScript setelah file HTML dan JavaScript diunduh dari server. Pada awalnya, hanya terdapat sedikit atau bahkan tidak ada tampilan yang dihasilkan di sisi klien sampai JavaScript menyelesaikan proses pembangunan tampilan. Hal ini bisa membuat waktu pemuatan halaman lebih lama karena memerlukan waktu untuk mengunduh dan mengeksekusi JavaScript.

Sedangkan pada SSR, tampilan dibangun di server dan diirimkan ke klien (browser) dalam bentuk file HTML. Sebelum tampilan HTML dikirimkan ke klien, server akan melakukan proses render pada sisi server, yang memungkinkan server mengirimkan halaman HTML yang sudah terisi isi secara lengkap ke klien. Dengan demikian, pengguna akan mendapatkan tampilan yang lebih cepat karena tampilan sudah dibangun pada sisi server.

**Client-Side Rendering (CSR)** :
![CSR](https://res.cloudinary.com/practicaldev/image/fetch/s--NCZOc3Bh--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/0z8cpipm5vsjlj11s6rz.png ).

**Server-Side Rendering (SSR)** :
![SSR](https://res.cloudinary.com/practicaldev/image/fetch/s--H5Y7c9ZT--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/677s8w25jofc5g4xbuck.png ).

## Referensi

- <https://nextjs.org>
- <https://dev.to/pahanperera/visual-explanation-and-comparison-of-csr-ssr-ssg-and-isr-34ea>

# Axios Js

## Penjelasan

Axios adalah sebuah library JavaScript yang digunakan untuk melakukan HTTP request dari browser atau dari server menggunakan Node.js. Library ini dapat digunakan untuk mengambil dan mengirim data dari API atau server, dengan dukungan berbagai macam tipe data seperti JSON, XML, dan sebagainya.

Axios merupakan alternatif yang populer untuk menggunakan built-in fetch API yang sudah disediakan oleh JavaScript, karena memiliki fitur-fitur seperti handling request cancelation, transform request dan response data, intercepting request dan response, dan masih banyak lagi.

Axios juga memiliki dukungan untuk memanipulasi request header, seperti menambahkan auth token, dan mengirim data melalui request body dalam berbagai format seperti form data, JSON, dan sebagainya.

Dalam pengembangan aplikasi web modern, Axios sangat berguna untuk mempercepat proses pengembangan dengan menyederhanakan koneksi ke server dan menghandle HTTP request secara efektif.

## Referensi

- <https://axios-http.com>
- <https://github.com/axios/axios>

# Styles

## Tailwind CSS

Tailwind CSS adalah sebuah framework CSS yang dirancang untuk memudahkan proses styling pada aplikasi web. Tailwind menyediakan sejumlah class CSS yang sudah siap pakai dan dapat diterapkan pada HTML tanpa perlu menulis kode CSS manual. Dengan demikian, Tailwind mempercepat proses styling dan memudahkan pengembang untuk membuat tampilan aplikasi web yang konsisten dan responsif.

Tailwind CSS menggunakan konsep utility-first CSS, yaitu gaya penulisan CSS dimana setiap class hanya mengontrol satu properti CSS. Hal ini memungkinkan pengembang untuk membuat perubahan pada tampilan aplikasi web dengan cepat dan mudah, tanpa harus mengubah kode CSS manual.

Selain itu, Tailwind juga menyediakan fitur-fitur seperti customizability dan responsive design, sehingga memungkinkan pengembang untuk mengatur dan mengatur tampilan aplikasi web dengan mudah, bahkan pada berbagai layar dan ukuran.

Tailwind CSS juga menyediakan dokumentasi yang sangat lengkap, sehingga memudahkan pengembang untuk mempelajari dan menggunakan framework ini dengan cepat. Dalam pengembangan aplikasi web modern, Tailwind CSS sangat berguna untuk mempercepat proses pengembangan, menghemat waktu, dan meningkatkan produktivitas.

## DaisyUI

DaisyUI adalah sebuah library CSS yang dibangun di atas Tailwind CSS yang menyediakan komponen-komponen UI yang siap digunakan untuk mempercepat proses pengembangan aplikasi web. DaisyUI menyediakan sejumlah komponen UI yang siap pakai, seperti tombol, form, navigasi, dan banyak lagi, sehingga memudahkan pengembang untuk membangun tampilan aplikasi web dengan cepat dan konsisten.

## clsx

**`clsx`** adalah sebuah library JavaScript yang berguna untuk menggabungkan beberapa class name secara kondisional di dalam React. Dengan menggunakan clsx, kita bisa menentukan beberapa class name yang ingin digunakan pada suatu elemen dan menambahkannya berdasarkan kondisi tertentu. Contohnya, ketika kita ingin menambahkan class name pada suatu button jika terjadi kondisi loading, kita bisa menggunakan clsx untuk menggabungkan class name yang ada dengan class name yang baru.

Contoh penggunaan **`clsx`** dalam React:

```jsx
import clsx from 'clsx';

function MyButton(props) {
  const { isLoading } = props;
  const btnClass = clsx('btn', { loading: isLoading });

  return (
    <button className={btnClass}>
      {isLoading ? 'Loading...' : 'Click Me'}
    </button>
  );
}
```

Pada contoh di atas, **`clsx`** digunakan untuk menggabungkan class name **`'btn'`** dengan class name **`'loading'`** pada suatu button jika prop **`isLoading`** bernilai true. Jika **`isLoading`** bernilai **`false`**, maka class name **`'loading'`** tidak akan digunakan. Hal ini berguna untuk memberikan tampilan yang berbeda pada suatu button ketika sedang dalam keadaan loading.

## Referensi

- <https://tailwindcss.com>
- <https://daisyui.com>
- <https://www.npmjs.com/package/clsx>

# React Query

## Penjelasan

React Query adalah sebuah library untuk mengelola state pada aplikasi React dengan cara yang mudah, intuitif, dan performa tinggi. Library ini memudahkan developer untuk mengambil data dari server atau sumber lainnya, menyimpannya dalam cache, dan mengelola state data dengan mudah di dalam aplikasi React.

Dalam penggunaannya, React Query memanfaatkan konsep cache data untuk mempercepat akses data dari server. Saat aplikasi memerlukan data baru, React Query akan mencoba untuk mengambil data tersebut dari cache terlebih dahulu. Jika data tidak ditemukan di dalam cache, React Query akan melakukan permintaan ke server untuk mengambil data tersebut.

## Contoh Penggunaan

Berikut adalah contoh penggunaan React Query dalam aplikasi React:

```jsx
import { useQuery } from 'react-query';

function App() {
  const { isLoading, error, data } = useQuery({
    queryKey: ['todos'],
    queryFn: () =>
      fetch('https://jsonplaceholder.typicode.com/todos').then(res =>
        res.json()
      )
  });

  if (isLoading) return 'Loading...';

  if (error) return `Error: ${error.message}`;

  return (
    <div className="container mx-auto justify-center flex">
      <div>
        <h1 className="text-xl my-3">Todos :</h1>
        <ul className="menu bg-base-300">
          {data.map(todo => (
            <li key={todo.id}>
              <a>{todo.title}</a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
```

Pada contoh di atas, kita menggunakan hook `useQuery` dari React Query untuk mengambil data dari URL <https://jsonplaceholder.typicode.com/todos> dan menyimpannya dalam cache dengan key `todos`. Jika data sudah ada di dalam cache, React Query akan mengambil data tersebut dari cache. Jika tidak, React Query akan melakukan permintaan ke server untuk mengambil data tersebut.

Dalam contoh tersebut, kita juga menggunakan variabel `isLoading`, `error`, dan `data` untuk mengelola state `data`. Variabel `isLoading` digunakan untuk menampilkan pesan loading saat `data` masih diambil dari server. Variabel `error` digunakan untuk menampilkan pesan `error` jika terjadi `error` saat mengambil `data` dari server. Variabel `data` digunakan untuk menampilkan `data` yang berhasil diambil dari server.

## Contoh Penggunaan Pada Server-side rendering (SSR)

Dalam penggunaan SSR, kita membuat instance dari QueryClient pada server side dan mem-passing data state dari server ke client untuk kemudian memanfaatkan kembali state tersebut, sehingga state tidak perlu di-fetch ulang ketika client mengakses halaman.

Berikut ini adalah contoh penggunaan React Query dengan server-side rendering (SSR) menggunakan hook useHydrate.

```jsx
import { useHydrate, QueryClient, QueryClientProvider, useQuery } from 'react-query';
import { dehydrate } from 'react-query/hydration';

export async function getServerSideProps() {
  const queryClient = new QueryClient();

  await queryClient.prefetchQuery({
    queryKey: ['todos'],
    queryFn: () =>
      fetch('https://jsonplaceholder.typicode.com/todos').then(res =>
        res.json()
      )
  });

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
}

function Todos() {
  const { data: todos, isLoading } = useQuery({
    queryKey: ['todos'],
    queryFn: () =>
      fetch('https://jsonplaceholder.typicode.com/todos').then(res =>
        res.json()
      )
  });

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <div className="container mx-auto justify-center flex">
      <div>
        <h1 className="text-xl my-3">Todos :</h1>
        <ul className="menu bg-base-300">
          {data.map(todo => (
            <li key={todo.id}>
              <a>{todo.title}</a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

```

Pada contoh ini, kita menggunakan `useQuery` untuk mengambil data dari <https://jsonplaceholder.typicode.com/todos>. Kemudian kita menggunakan `useHydrate` pada Todos component untuk memhydrate data yang telah diambil pada server-side rendering dan kemudian disimpan pada dehydratedState. Data tersebut kemudian dapat digunakan pada client-side rendering secara cepat tanpa harus meminta data ke server lagi. Pada `getServerSideProps`, kita menggunakan `prefetchQuery` untuk mengambil data pada server-side rendering dan menyimpan data tersebut pada queryClient.

## Referensi

- <https://tanstack.com/query/v4>
- [Dokumentasi React Query + SSR](https://tanstack.com/query/latest/docs/react/guides/ssr#using-hydration)


