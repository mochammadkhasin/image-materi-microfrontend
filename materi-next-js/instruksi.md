<h1>02-Instruksi-Materi-NextJS</h1>

Gitlab Repository : https://gitlab.com/sekjen-dpr/workshop/materi-next-js/ didalam folder /Materi

# Implementasi Axios

## 1. Install Axios dari Packages Manager

Pastikan Anda berada di **ROOT Folder**
Install packages `Axios` dengan menjalankan command :

```bash
yarn add axios
```

## 2. Buat instance `Axios`

Instance **Axios** akan digunakan pada setiap request api.

Pada path **`utils/helper`** buat folder dengan nama **`gql-http-client`** lalu di dalam folder tersebut buatlah file dengan nama **`index.js`**

### 1. Menentukan Base Url untuk API

Kita akan membuat variable **baseURL** yang akan menentukan **URL** untuk merequest **api**

```javascript
import getConfig from 'next/config';
const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();
const baseURL =
  typeof window === 'undefined'
    ? serverRuntimeConfig.apiURL
    : publicRuntimeConfig.apiURL;
```

- **`import getConfig from 'next/config';`**: Kode ini digunakan untuk mengimport library **`getConfig`** dari package **`next/config`**. Library ini digunakan untuk mengakses konfigurasi yang ada pada Next.js.

- **`const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();`**: Kode ini mengambil konfigurasi dari Next.js, yang terdiri dari **`publicRuntimeConfig`** dan **`serverRuntimeConfig`**. Konfigurasi tersebut nantinya akan digunakan sebagai basis URL API.

- **`const baseURL = typeof window === 'undefined' ? serverRuntimeConfig.apiURL : publicRuntimeConfig.apiURL;`**: Kode ini digunakan untuk menentukan URL dari API yang akan digunakan oleh Axios. Jika kode ini dijalankan di server-side Next.js, maka **`serverRuntimeConfig`** akan digunakan. Namun jika dijalankan di client-side, maka **`publicRuntimeConfig`** akan digunakan.  **`serverRuntimeConfig`** dan **`publicRuntimeConfig`** akan dikonfigurasikan pada file **`next.config.js`** :

  ```javascript
  const nextConfig = {
    rewrites: async () => [
      {
        source: '/gql',
        destination: 'https://api-portal-staging.dpr.go.id/web/graphql'
      }
    ],
    publicRuntimeConfig: {
      apiURL: '/gql'
    },
    serverRuntimeConfig: {
      apiURL: 'https://api-portal-staging.dpr.go.id/web/graphql'
    }
  };
  ```

### 2. Membuat Instance Axios

```javascript
const axiosInstance = axios.create({
  baseURL,
  headers: { 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' }
});
```

- **`const axiosInstance = axios.create({ ... });`**: Kode ini digunakan untuk membuat instance dari Axios dengan menggunakan URL dasar API yang telah ditentukan sebelumnya. Di sini juga ditentukan konfigurasi **`headers`** yang akan digunakan pada setiap permintaan HTTP, yaitu **`'Content-Type': 'application/json'`** dan **`'Cache-Control': 'no-cache'`**.

### 3. Membuat Fungsi HttpClient

```javascript
const gqlHttpClient = ({ query, variables } = {}) => {
  const body = {
    query,
    variables
  };
  return axiosInstance.post('', body).then(res => res.data.data);
};
```

- **`const gqlHttpClient = ({ query, variables } = {}) => { ... }`**: Karena kita menggunakan **GraphQL**, maka kita hanya menggunakan method **`POST`**. Fungsi **`gqlHttpClient`** yang menerima dua parameter opsional, yaitu **`query`** dan **`variables`**. Fungsi ini akan membuat objek **`body`** yang berisi query dan variabel yang dikirimkan ke server, kemudian melakukan permintaan POST menggunakan Axios dengan menggunakan URL yang telah ditentukan sebelumnya.

### 4. Menghandle error pada GraphQL

Dikarenakan kita menggunakan **GraphQL** maka error yang terima oleh client tetap **`success`** (status code: 200) tetapi dengan message error. Maka kita akan mengatur agar mengembalikan error jika memang ada error dari request yang kita kirimkan. Kita akan menggunakan fitur **`interceptors`** dari **Axios**.

```javascript
axiosInstance.interceptors.response.use(response => {
  const resultErrors = response.data?.errors;
  if (resultErrors && resultErrors.length > 0) {
    return Promise.reject(resultErrors);
  }
  return response;
});
```

- **`axiosInstance.interceptors.response.use(response => { ... });`**: Kode ini digunakan untuk menambahkan interceptors pada instance Axios yang telah dibuat sebelumnya. Pada interceptors ini, akan dicek apakah response yang diterima mengandung error atau tidak. Jika ada error, maka permintaan akan ditolak dan error akan dijadikan argumen pada Promise.reject. Jika tidak ada error, maka response akan dikembalikan.

### 5. Meng-export fungsi gqlHttpClient

```javascript
export default gqlHttpClient;
```

- **`export default gqlHttpClient;`**: Kode ini digunakan untuk mengekspor fungsi **`gqlHttpClient`** agar dapat digunakan pada file JavaScript lainnya.

## Hasil akhir dari file `gql-http-client/index.js`

```jsx
import axios from 'axios';
import getConfig from 'next/config';
const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();
const baseURL =
  typeof window === 'undefined'
    ? serverRuntimeConfig.apiURL
    : publicRuntimeConfig.apiURL;

const axiosInstance = axios.create({
  baseURL,
  headers: { 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' }
});
const gqlHttpClient = ({ query, variables } = {}) => {
  const body = {
    query,
    variables
  };
  return axiosInstance.post('', body).then(res => res.data.data);
};

axiosInstance.interceptors.response.use(response => {
  const resultErrors = response.data?.errors;
  if (resultErrors && resultErrors.length > 0) {
    return Promise.reject(resultErrors);
  }
  return response;
});

export default gqlHttpClient;
```
# Service Api Banner

## 1. Membuat service API Banner

Untuk membuat service pada aplikasi ini semua ada di dalam folder **`api/**`**.

Kemudian kita akan membuat folder **`banner`** dengan file **`index.js`** didalam nya.

```javascript
import gqlHttpClient from 'utils/helper/gql-http-client';

/**
 * Mengambil daftar Hero Banner dari API.
 *
 * @param {Object} [params] Objek parameter opsional.
 * @param {number} [params.perPage=5] Jumlah data yang ingin diambil dalam satu halaman.
 * @param {number} [params.page=1] Halaman yang ingin diambil.
 *
 * @return {Promise<Array<string>>} Daftar Banner.
 */
export const getDaftarBanner = async ({
  perPage = 5,
  page = 1,
  isError = false
} = {}) => {
  const query = `query getDaftarHero($page: Int, $first: Int!) {
    getDaftarHero(page: $page, first: $first) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        id
        fileLink
      }
      ${isError ? 'error' : ''}
    }
  }
  `;
  const variables = {
    page,
    first: perPage
  };
  const res = await gqlHttpClient({ query, variables });

  return res.getDaftarHero.data;
};
```

- Import module **`gqlHttpClient`** yang berisi fungsi untuk melakukan HTTP request menggunakan GraphQL.
- Buat fungsi **`getDaftarBanner`** dengan parameter opsional **`params`**
  - Destructure parameter **`params`** dan berikan nilai default
  - Buat variabel **`query`** dengan template literal yang berisi query GraphQL untuk mengambil daftar hero banner dengan beberapa properti, yaitu:
    - **`paginatorInfo`** yang akan mengambil informasi tentang halaman (page) dan jumlah data per halaman (per page)
    - **`data`** yang akan mengambil array dari id dan link file banner
    - **`error`** yang akan diambil jika parameter isError bernilai true
  - Buat variabel **`variables`** untuk menyimpan parameter **`page`** dan **`perPage`**
  - Lakukan panggilan ke **`gqlHttpClient`** dengan parameter query dan variables, lalu simpan hasilnya di variabel **`res`**
  - Kembalikan array dari data banner yang diperoleh dari objek **`res.getDaftarHero`**

## 2. Memakai konstan paginatorInfo

Karena semua request api dengan pagination akan mengirimkan query dengan **`paginatorInfo`** yang sama, maka agar lebih efisien, kita akan membuat 1 file khusus untuk mengatur konstan **`paginatorInfo`** tersebut.

Kita akan membuat file **`gql.js`** dalam folder **`utils/constants`** 

```bash
utils/
└── constants/
    └── gql.js
```

dengan isi :

```javascript
export const paginatorInfo = `
  paginatorInfo {
    count
    currentPage
    firstItem
    hasMorePages
    lastItem
    lastPage
    perPage
    total
  }
`;
```

Lalu kita akan sedikit mengubah pada service banner untuk memakai konstant tersebut

```diff
....
+ import { paginatorInfo } from 'utils/constants/gql';
export const getDaftarBanner = async ({
  perPage = 5,
  page = 1,
  isError = false
} = {}) => {
  const query = `query getDaftarHero($page: Int, $first: Int!) {
    getDaftarHero(page: $page, first: $first) {
-      paginatorInfo {
-        count
-        currentPage
-        firstItem
-        hasMorePages
-        lastItem
-        lastPage
-        perPage
-        total
-      }
+     ${paginatorInfo}
      data {
        id
        fileLink
      }
      ${isError ? 'error' : ''}
    }
  }
....
};
```

## Hasil Akhir dari file service Banner

```javascript
import gqlHttpClient from 'utils/helper/gql-http-client';
import { paginatorInfo } from 'utils/constants/gql';

/**
 * Mengambil daftar Hero Banner dari API.
 *
 * @param {Object} [params] Objek parameter opsional.
 * @param {number} [params.perPage=5] Jumlah data yang ingin diambil dalam satu halaman.
 * @param {number} [params.page=1] Halaman yang ingin diambil.
 *
 * @return {Promise<Array<string>>} Daftar Banner.
 */
export const getDaftarBanner = async ({
  perPage = 5,
  page = 1,
  isError = false
} = {}) => {
  console.log('isError: ', isError);
  const query = `query getDaftarHero($page: Int, $first: Int!) {
    getDaftarHero(page: $page, first: $first) {
      ${paginatorInfo}
      data {
        id
        fileLink
      }
      ${isError ? 'error' : ''}
    }
  }
  `;
  const variables = {
    page: page ?? 1,
    first: perPage ?? 5
  };
  const res = await gqlHttpClient({ query, variables });

  return res.getDaftarHero.data;
};

```
# Implementasi React Query

## 1. Install react-query dari Packages Manager

Pastikan Anda berada di **ROOT Folder**
Install packages `react-query` dengan menjalankan command :

```bash
yarn add react-query
```

## 2. Setup react-query

```diff
// File : pages/_app.js
+ import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

+ const queryClient = new QueryClient({
+  defaultOptions: {
+    queries: {
+      staleTime: 5000,
+      retry: (failureCount, error) => {
+        if (error.status === 404) return false;
+        else if (failureCount < 2) return true;
+        else return false;
+      },
+      refetchOnWindowFocus: false
+    }
+  }
+});

function MyApp({ Component, pageProps }) {
  return (
+    <QueryClientProvider client={queryClient}>
       <Component {...pageProps} />
+    </QueryClientProvider>
  );
}

export default MyApp;
```

- **`import { QueryClient, QueryClientProvider } from '@tanstack/react-query';`**: mengimport **`QueryClient`** dan **`QueryClientProvider`** dari paket **`@tanstack/react-query`**.
- **`const queryClient = new QueryClient({...});`**: membuat instance baru dari **`QueryClient`** dengan **`Default Option`** yang akan digunakan untuk mengirim permintaan ke server. **`Default Option`** yang digunakan ketika melakukan query menggunakan **`QueryClient`**:
  - **`staleTime`**: Menentukan berapa lama data di-cache sebelum data tersebut dianggap tidak lagi up-to-date
  - **`retry`**: Menentukan apakah query perlu di-retry ketika terjadi error, berdasarkan **`failureCount`** (jumlah percobaan yang sudah dilakukan) dan **`error`** (objek error yang dihasilkan)
  - **`refetchOnWindowFocus`**: Menentukan apakah query akan di-fetch ulang ketika window focus berubah (misalnya ketika pengguna beralih tab browser)
- **`<QueryClientProvider client={queryClient}>`**: sebuah tag yang digunakan untuk menyediakan instance dari **`queryClient`** sehingga dapat diakses di dalam komponen lain pada aplikasi.

### Hasil akhir dari file `pages/_app.js`

```jsx
import '../styles/globals.css';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <Component {...pageProps} />
    </QueryClientProvider>
  );
}

export default MyApp;

```

## 3. Memakai react-query untuk mengambil data dari server

Pada section ini kita akan mengubah file **`pages/index.js`** untuk mengambil data dari api menggunakan library **`react-query`**

```diff
+ import { useQuery } from '@tanstack/react-query';
+ import { getDaftarBanner } from 'api/banner';

export default function Home() {
+  const [param, setParam] = useState({});
+  const { data, isFetching, error } = useQuery({
+    queryKey: ['daftarBanner', { ...param }],
+    queryFn: async ({ queryKey }) => {
+      const [, params] = queryKey;
+      return await getDaftarBanner(params);
+    }
+  });
  ....
}
```

- **`import { useQuery } from '@tanstack/react-query';`** mengimpor hook **`useQuery`** dari library **`react-query`** yang digunakan untuk melakukan fetching data secara mudah di aplikasi React. ref: [dokumentasi](https://tanstack.com/query/latest/docs/react/reference/useQuery)
- **`import { getDaftarBanner } from 'api/banner';`** mengimpor fungsi **`getDaftarBanner`** dari modul **`api/banner`** yang berfungsi untuk mengambil data daftar banner dari server.
- **`const [param, setParam] = useState({});`** membuat state **`param`** dan **`setParam`** dengan nilai awal berupa objek kosong **`{}`** yang akan digunakan untuk mengatur parameter query yang akan dikirimkan ke server.
- **`useQuery`** adalah hook yang digunakan untuk melakukan fetch data dengan React Query, berisi parameter-object yang memiliki key **`queryKey`** dan **`queryFn`**.
  - **`queryKey`** adalah array yang digunakan sebagai kunci query. Di sini, key-nya adalah **`'daftarBanner'`** dan nilai kedua adalah **`param`**.
  - **`queryFn`** adalah fungsi yang digunakan untuk melakukan query dengan parameter yang diberikan dan mengembalikan promise dengan data yang diambil. Di sini, parameter query didefinisikan sebagai **`{...param}`** dan menggunakan fungsi **`getDaftarBanner`** untuk mengambil data daftar banner dengan parameter tersebut dari server.
- **`const { data, isFetching, error } = useQuery({...});`** Memanggil hook **`useQuery`** dengan parameter objek yang telah ditentukan sebelumnya, dan mendapatkan hasil query dalam variabel **`data`**, **`isFetching`**, dan **`error`**. Variabel data akan berisi hasil dari query jika sukses, **`isFetching`** akan berisi **`true`** ketika query sedang berjalan, dan error akan berisi pesan error jika query gagal.

Setelah **`react-query`** di-setup, Kita akan mulai meimplementasikan state **`data`**, **`isFetching`**, dan **`error`** ke dalam ui.

```diff
<Card className="bg-base-300 w-[500px] shadow-xl">
  <h4 className="text-2xl font-bold">Load Hero Banner</h4>
  <div className="card-actions my-4">
-   <button className={clsx('btn btn-primary')} onClick={()=> console.log('Load Data')}>
+   <button className={clsx('btn btn-primary', isFetching && 'loading' )} onClick={()=> setParam({ isError: false })}>
      Load Data
    </button>
-   <button className={clsx('btn btn-error')} onClick={()=> console.log('Load Data Error')}>
+   <button className={clsx('btn btn-error', isFetching && 'loading' )} onClick={()=> setParam({ isError: true })}>
      Load Data with Error
    </button>
  </div>
- <div className="overflow-hidden bg-base-100 rounded p-3">
-   <code className="block whitespace-pre overflow-x-scroll pb-5">
-     {JSON.stringify({ dummy: 'ini dummy data' }, null, 2)}
-   </code>
- </div>
+ {data && (
+   <div className="overflow-hidden bg-base-100 rounded p-3">
+     <code className="block whitespace-pre overflow-x-scroll pb-5">
+       {JSON.stringify(data, null, 2)}
+     </code>
+   </div>
+ )}
+ {error && (
+   <div className="overflow-hidden bg-error-content rounded p-3">
+     <code className="block whitespace-pre overflow-x-scroll pb-5">
+       {JSON.stringify(error, null, 2)}
+     </code>
+   </div>
+ )}
<Card/>
```

- Pada baris ke-4, ditambahkan **`isFetching && 'loading'`** ke dalam **`className`** dari button pertama agar tombol tersebut dapat menampilkan indikator loading saat data sedang diambil.
- Pada baris ke-8, ditambahkan **`isFetching && 'loading'`** ke dalam **`className`** dari button kedua agar tombol tersebut dapat menampilkan indikator loading saat data sedang diambil.
- Pada baris ke-13-31, ditambahkan **`{data && (`** dan **`{error && (`** agar blok kode yang menghasilkan data dan error tersebut hanya ditampilkan jika ada data atau error yang diterima.

## Hasil Akhir dari file `pages/index.js`

```jsx
import Card from 'views/components/card';
import { useQuery } from '@tanstack/react-query';
import { getDaftarBanner } from 'api/banner';
import { useState } from 'react';

export default function Home() {
  const [param, setParam] = useState({});
  const { data, isFetching, error } = useQuery({
    queryKey: ['daftarBanner', { ...param }],
    queryFn: async ({ queryKey }) => {
      const [, params] = queryKey;
      return await getDaftarBanner(params);
    }
  });
  return (
    <div>
      <main className="container mx-auto justify-center flex">
        <Card className="bg-base-300 w-[500px] shadow-xl">
          <h4 className="text-2xl font-bold">Load Hero Banner</h4>
          <div className="card-actions my-4">
            <button
              className={clsx('btn btn-primary', isFetching && 'loading')}
              onClick={() => setParam({ isError: false })}
            >
              Load Data
            </button>
            <button
              className={clsx('btn btn-error', isFetching && 'loading')}
              onClick={() => setParam({ isError: true })}
            >
              Load Data with Error
            </button>
          </div>
          {data && (
            <div className="overflow-hidden bg-base-100 rounded p-3">
              <code className="block whitespace-pre overflow-x-scroll pb-5">
                {JSON.stringify(data, null, 2)}
              </code>
            </div>
          )}
          {error && (
            <div className="overflow-hidden bg-error-content rounded p-3">
              <code className="block whitespace-pre overflow-x-scroll pb-5">
                {JSON.stringify(error, null, 2)}
              </code>
            </div>
          )}
        </Card>
      </main>
    </div>
  );
}
```

# react-query SSR

## 1. Setup react-query pada `Server-Side Rendering (SSR)`

```diff
import '../styles/globals.css';
-import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
+import {
+  QueryClient,
+  QueryClientProvider,
+  Hydrate
+} from '@tanstack/react-query';
import { useState } from 'react';

-  const queryClient = new QueryClient({
-    defaultOptions: {
-      queries: {
-        staleTime: 5000,
-        retry: (failureCount, error) => {
-          if (error.status === 404) return false;
-          else if (failureCount < 2) return true;
-          else return false;
-        },
-        refetchOnWindowFocus: false
-      }
-    }
-  });
function MyApp({ Component, pageProps }) {
+  const [queryClient] = useState(
+    () =>
+      new QueryClient({
+        defaultOptions: {
+          queries: {
+            staleTime: 5000,
+            retry: (failureCount, error) => {
+              if (error.status === 404) return false;
+              else if (failureCount < 2) return true;
+              else return false;
+            },
+            refetchOnWindowFocus: false
+          }
+        }
+      })
+  );
  return (
    <QueryClientProvider client={queryClient}>
+      <Hydrate state={pageProps.dehydratedState}>
        <Component {...pageProps} />
+      </Hydrate>
    </QueryClientProvider>
  );
}
```

- Pada kode diatas menunjukkan bahwa kita menambahkan import untuk **`Hydrate`** dari **`@tanstack/react-query`**.
- Selanjutnya, kita menggunakan **`useState`** untuk menginisialisasi queryClient sebagai instance dari **`QueryClient`**.
- Selanjutnya, kita menambahkan komponen **`Hydrate`** dengan props **`state={pageProps.dehydratedState}`**. Komponen ini memungkinkan data dari server-side rendering (SSR) disimpan pada client-side sehingga data dapat dihydrate ketika aplikasi di-render ulang pada client-side.

ref: <https://tanstack.com/query/v4/docs/react/guides/ssr#using-hydration>

## 2. Implementasi react-query pada `Server-Side Rendering (SSR)`

```diff
....
- import { dehydrate, QueryClient, useQuery } from '@tanstack/react-query';
+ import { dehydrate, QueryClient, useQuery } from '@tanstack/react-query';

+ export async function getServerSideProps() {
+  const queryClient = new QueryClient();
+  await queryClient.prefetchQuery({
+    queryKey: ['daftarBanner', {}],
+    queryFn: async () => await getDaftarBanner({})
+  });
+
+  return {
+   props: {
+      dehydratedState: dehydrate(queryClient)
+    }
+  };
+}
```

- **`import { dehydrate, QueryClient, useQuery } from '@tanstack/react-query';`** mengimport beberapa library yang digunakan dalam code block ini, yaitu dehydrate, QueryClient, dan useQuery dari package @tanstack/react-query.
- **`export async function getServerSideProps()`** merupakan sebuah fungsi async yang digunakan oleh Next.js untuk melakukan proses fetching data pada server-side. Fungsi ini akan dipanggil terlebih dahulu sebelum render halaman pada server-side.
- Selanjutnya kita akan melakukan prefetch untuk melakukan teknik **`Hydration`**, lalu data yang didapat dari hasil request ini akan dikirimkan dan di implementasikan ke **`QueryClient`** yang ada di client side (**`pages/_app.js`**).

## Hasil Akhir dari file **`pages/_app.js`**

```jsx
import '../styles/globals.css';
import {
  QueryClient,
  QueryClientProvider,
  Hydrate
} from '@tanstack/react-query';
import { useState } from 'react';

function MyApp({ Component, pageProps }) {
  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            staleTime: 5000,
            retry: (failureCount, error) => {
              if (error.status === 404) return false;
              else if (failureCount < 2) return true;
              else return false;
            },
            refetchOnWindowFocus: false
          }
        }
      })
  );
  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <Component {...pageProps} />
      </Hydrate>
    </QueryClientProvider>
  );
}

export default MyApp;
```

## Hasil Akhir dari file **`pages/index.js`**

```jsx
import Card from 'views/components/card';
import { dehydrate, QueryClient, useQuery } from '@tanstack/react-query';
import { getDaftarBanner } from 'api/banner';
import clsx from 'clsx';
import { useState } from 'react';

export async function getServerSideProps() {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery({
    queryKey: ['daftarBanner', {}],
    queryFn: async () => await getDaftarBanner({})
  });

  return {
    props: {
      dehydratedState: dehydrate(queryClient)
    }
  };
}
....
```
