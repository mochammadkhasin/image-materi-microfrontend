<h1>03-Penjelasan-Materi-Microfrontend</h1>

Gitlab Repository : https://gitlab.com/sekjen-dpr/workshop/materi-microfrontend didalam folder /Materi

# Microfrontend

## Penjelasan

**Microfrontend** adalah salah satu pendekatan dalam pengembangan aplikasi web front-end dengan memecah atau memisahkan aplikasi menjadi beberapa bagian kecil yang lebih mudah dikelola dan dikembangkan. Setiap bagian kecil dapat diimplementasikan secara independen, baik oleh satu tim atau oleh beberapa tim yang berbeda. Dalam konteks Javascript, Javascript Microfrontend mengacu pada penggunaan bahasa pemrograman Javascript dan teknologi terkait seperti React, Angular, atau Vue.js untuk membangun dan mengintegrasikan microfrontends dalam aplikasi web.

## Module Federation Webpack

Apa Itu **`Module Federation`** ?

- **`Module Federation`** salah satu fitur dari Webpack 5 yang memungkinkan aplikasi Javascript untuk memuat kode aplikasi Javascript lain pada saat runtime di browser, dan juga berbagi dependensi. Jika suatu aplikasi memerlukan dependensi yang sama dengan aplikasi lain, maka Webpack akan mendownload dependensi tersebut di saat salah satu dari aplikasi tersebut dimuat pertama.

Untuk aplikasi ini, **`Module Federation`** dibagi menjadi 2 tipe module yaitu :

- **`Host`** : Webpack Build yang dipanggil browser pertama kali pada saat memuat halaman website
- **`Remote`** : Webpack Build lain yang akan menjadi bagian dan dikonsumsi oleh **`“Host”`**

**`Module Federation`** di design supaya setiap build/app bisa berdiri sendiri, dan di deploy secara independent

Berdasar dan penjelasan sebelum nya, berikut adalah gambaran Module Federation dalam aplikasi ini :

![Module Federation](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-02.png)

### Isolation Mode

Module Federation memungkinkan kita mengerjakan suatu module secara independent. Pada module tipe remote, pada saat development kita bisa mengerjakan tanpa dependensi atau ketergantungan dari module-module lainnya, itulah yang kita sebut Isolation Mode.

- Isolation Mode

![Fork](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-19.jpg)

- Di dalam Host

![Fork](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-20.jpg)

## Microfrontend Routing

Dengan memanfaatkan fitur **`Lazy load module`** dari React dan juga third-party library **`react-router-dom`**, container bisa mengatur hanya mendownload/load module jika module tersebut diperlukan saja.

Untuk memastikan Routing berjalan dengan benar pada aplikasi micro frontend, diperlukan 2 tipe history pada routing yaitu **`Browser History`** dan **`Memory History`**. Browser history untuk modul bertipe **`Host`** dan Memory History untuk modul bertipe **`Remote`**.

**`Browser History`** adalah jenis history yang melacak apa yang user sedang kunjungi dengan melihat path setelah domain. Contoh nya pada url https:/app.com/ user/login, /user/login adalah apa yang user sedang kunjungi pada saat itu.
Selain itu Memory history melacak apa yang sedang user kunjungi didalam memory.

Dengan memanfaatkan jenis-jenis history itulah third-party library **`react-router-dom`** menentukan halaman apa yang akan ditampilkan ke user. Begitu pula dengan router yang digunakan oleh modern javascript framework seperti vue atau angular.

Dan berikut adalah contoh jika ingin menambahkan ExampleApp dengan route **`/example/**`\*\*

![Example App](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-03.png)

Terlihat pada gambar diatas untuk baris ke 1 **`ExampleApp`** dimuat dengan cara lazyload dengan memanfaatkan fitur react yaitu **`Lazy load module`**. Pada baris ke 9-15 adalah bagaimana cara untuk mengatur url /example/\*\* agar menampilkan modul ExampleApp

## Autentikasi Microfrontend

Autentikasi pada aplikasi ini menggunakan protokol JSON Web Token(JWT). JWT akan dikirim dari client ke api melalui request header dengan format **`authorization: Bearer {Token}`**. Agar bisa dipakai semua modul maka **`token`** yang dihasilkan akan disimpan di dalam localstorage dengan key token, begitu juga dengan **`refreshToken`** akan disimpan di localstorage dengan key **`refreshToken`**.

![local storage](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-04.png)

Setiap token memiliki masa berlaku dengan apa yang telah disetel di keycloak, ketika masa berlaku token habis dan kita mengirim request api aplikasi ini di design untuk meminta token baru dengan meminta ulang token menggunakan refreshToken, jika refreshToken valid dan token baru diterima maka aplikasi akan mengirim ulang request api yang tadi gagal. Namun jika refreshToken tidak valid aplikasi akan otomatis logout.

Berikut adalah flow untuk proses ini.

![Flow Autentikasi](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-05.jpg)

## Konfigurasi Webpack di Microfrontend

Berikut adalah letak dimana kode konfigurasi berada :

```bash
MODULE/
└── config/
    ├── webpack.common.js  ← konfigurasi webpack untuk general
    ├── webpack.dev.js  ← konfigurasi webpack untuk development
    └── webpack.prod.js ← konfigurasi webpack untuk production
```

- **webpack.common.js**

  File ini di dedikasikan untuk konfigurasi webpack untuk general seperti compiler typescript, compiler sass/scss, compiler react, dotenv.

- **webpack.dev.js**

  File ini berisi konfigurasi untuk masa development aplikasi ini. Didalam terdapat konfigurasi untuk **`Module Federation`**, Konfigurasi port, proxy untuk development dan konfigurasi lainnya.

- **webpack.prod.js**

  File ini berisi konfigurasi untuk masa production aplikasi ini. Didalam terdapat konfigurasi untuk **`Module Federation`**, Konfigurasi port, proxy untuk development dan konfigurasi lainnya.

# State Management (redux-toolkit, rtk-query)

## Penjelasan 

**State management** adalah konsep untuk mengelola dan menyimpan data state pada aplikasi, khususnya pada aplikasi yang kompleks dan memiliki banyak komponen. **Redux Toolkit** dan **RTK-Query** adalah dua library yang dapat membantu developer dalam mengimplementasikan state management pada aplikasi web.

**Redux Toolkit** adalah sebuah library yang menggabungkan beberapa fungsi-fungsi yang sering digunakan pada Redux dan menyederhanakan cara kita dalam mengelola state. **Redux Toolkit** menyediakan fitur-fitur seperti createSlice, createAsyncThunk, configureStore, dan createEntityAdapter yang dapat memudahkan pengembang dalam mengatur state pada aplikasi. **Redux Toolkit** juga memudahkan dalam pembuatan reducer yang lebih mudah dibaca dan ditulis.

Sementara itu, **RTK-Query** adalah sebuah library yang memudahkan pengembang dalam mengatur request data ke server dan mengelola state pada aplikasi. **RTK-Query** memanfaatkan **Redux Toolkit** dan menyediakan fitur-fitur seperti automatic caching, automatic refetching, polling, dan banyak lagi. Dengan **RTK-Query**, pengembang dapat mengatur state dan request data dengan lebih mudah dan cepat, serta menghindari boilerplate code pada aplikasi.

# Typescript

## Penjelasan

**Typescript** adalah Javascript Superset.
Yang dimaksud Javascript Superset disini adalah bahasa pemrograman yang dibuat untuk membangun Javascript, dengan menambahkan fitur-fitur yang dimanfaatkan untuk menghasilkan kode javascript yang baik dan benar. Bisa dibilang **Typescript** adalah “Better version of javascript”.

**Typescript Environment**: 
![Typescript Environment](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-06.jpg)

## Kenapa Typescript?

![example ts 1](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-07.png)

**Masalah :**
Di runtime Javascript akan menggabung 2 string menjadi “12”, bukan mengkalkulasi 2 string tersebut menjadi “3”

**Pencegahan :**
Dengan **Typescript** kita dapat mencegah masalah tersebut pada saat mode development dengan menggunakan Type Checking yang akan mendeteksi dini kemungkinan-kemungkinan error, **Typescript** akan membantu kita untuk menulis kode Javascript yang lebih baik dan menghindari masalah-masalah seperti diatas

![example ts 2](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-08.jpg)

# Styles

## Implementasi CSS (Metronic theme(Bootstarp 5.2))

Untuk CSS kita akan menggunakan [Metronic theme](https://keenthemes.com/metronic/) yang dimana dalam theme tersebut menggunakan Framework CSS [Bootstarp versi 5.2](https://getbootstrap.com/docs/5.2).

Pada implementasinya kita membuat package library yaitu **`@sekjen-dpr/assets`** yang akan di host oleh gitlab.com, yang pada akhir nya kita akan memuat library tersebut di setiap module pada aplikasi ini. Namun untuk memuat library **`@sekjen-dpr/assets`** kita harus mengubah url registry dari default(<https://registry.npmjs.org>) ke <https://gitlab.com/api/v4/packages/npm/> dengan cara manambahkan konfigurasi pada file **`.npmrc`**.

![npm](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-09.png)

## Design Systems

Berdasarkan arsitektur **`microfrontend`** pada aplikasi ini maka kita akan membuat UI-Library yang akan dipakai pada setiap module pada aplikasi ini untuk mempermudah proses development atau maintenance kedepannya. UI-Library untuk design system yang kita buat untuk aplikasi ini adalah **`@sekjen-dpr/react`**, untuk bisa mengakses package tersebut kita harus mengubah default registry pada npm dengan menambahkan konfigurasi pada file **`.npmrc`** pada setiap module sama seperti yang dijelaskan pada bagian implementasi css.

UI-Library yang kita buat adalah berdasarkan **`Metronic`** Theme dengan framework CSS **`Bootstrap versi 5.2`** dan kita menggunakan Metodologi **`Atomic Desain`** dalam membangunnya
