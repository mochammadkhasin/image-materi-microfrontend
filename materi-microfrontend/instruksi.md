<h1>04-Instruksi-Materi-Microfrontend</h1>

Gitlab Repository : https://gitlab.com/sekjen-dpr/workshop/materi-microfrontend didalam folder /Materi

# Persiapan

## Instalasi NPM package dari gitlab npm registry

Untuk bisa melakukan instalasi package design system (**`@sekjen-dpr/assets`** & **`@sekjen-dpr/react`**), kita harus mendaftarkan auth token dari gitlab ke konfigurasi npm lokal kita, caranya :

- Buat auth token di [https://gitlab.com/-/profile/personal_access_tokens](https://gitlab.com/-/profile/personal_access_tokens)

![npm](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-12.jpg)

- Setelah itu jalankan perntah berikut pada terminal

  ```bash
  npm config set -- //gitlab.com/api/v4/packages/npm/:_authToken=your_token
  ```

  - Ganti **`your_token`** dengan token yang sudah kita buat tadi

# Membuat Module Berita

Dalam materi ini kita akan membuat module Berita dengan fitur :

- Daftar Berita
- Tambah Berita
- Ubah Bertia
- Hapus Berita

Kita akan menggunakan [Boilerplate React](https://gitlab.com/sekjen-dpr/microfrontend/boilerplate-react) untuk mempermudah kita membuat module

## 1. Download Source Code dari Repository [Boilerplate React](https://gitlab.com/sekjen-dpr/microfrontend/boilerplate-react)

![Example App](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-10.jpg)

Setelah itu extract file **`zip`** tersebut dan letakan didalam **`root folder`** dengan nama **`berita`**

<img src="https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-11.jpg" alt="Folder Berita"  width="200"/>

Lalu kita akan melakukan instalasi **npm packages** pada folder **`berita`** jalankan perintah :

```bash
pnpm install
```

## 2. Konfigurasi Remote Module

- Rename file **`.env.example`** pada **`Root folder berita`** menjadi **`.env`**, **`.env`** berguna untuk menyimpan environment variables yang akan digunakan dalam Module ini. contoh:

- ubah konfigurasi untuk development environment pada file **`berita/config/webpack.dev.js`**

  ```diff
  const devConfig = {
    mode: "development",
    output: {
  -   publicPath: "http://localhost:3001/",
  +   publicPath: "http://localhost:3004/",
    },
    devServer: {
  -   port: 3001,
  +   port: 3004,
      historyApiFallback: true,
      proxy: {
        "/api": {
          target: process.env.API_DEV_PROXY_TARGET_URL,
          pathRewrite: { "^/api": "" },
        },
      },
      hot: false,
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./public/index.html",
      }),
      new ModuleFederationPlugin({
  -     name: "example",
  +     name: "berita",
        filename: "remoteEntry.js",
        exposes: {
  -       "./ExampleApp": "./src/bootstrap",
  +       "./BeritaApp": "./src/bootstrap",
        },
        shared: packageJson.dependencies,
      }),
      new HtmlWebpackPlugin({
        template: "./public/index.html",
      }),
    ],
  };
  ```

  - **`publicPath`** diubah dari <http://localhost:3001/> menjadi <http://localhost:3004/>, untuk menentukan public URL dari bundle assets module ini
  - Pada objek **`devServer`**, properti **`port`** diganti nilai nya dari **`3001`** menjadi **`3004`**.
  - Pada objek **`ModuleFederationPlugin`**, properti name diganti nilai nya dari **`"example"`** menjadi **`"berita"`**, sedangkan properti exposes diganti nilai nya dari **`"./ExampleApp"`** menjadi **`"./BeritaApp"`**, ini adalah konfigurasi bagaimana **`Remote Module Berita`** ini akan dikenali oleh **`Host Module`**

- ubah konfigurasi untuk development environment pada file **`berita/config/webpack.prod.js`**

  ```diff
  const prodConfig = {
    mode: "production",
    output: {
      filename: "[name].[contenthash].js",
  -   publicPath: "/example/latest/",
  +   publicPath: "/berita/latest/",
      clean: true,
    },
    plugins: [
      new ModuleFederationPlugin({
  -     name: "example",
  +     name: "berita",
        filename: "remoteEntry.js",
        exposes: {
  -       "./ExampleApp": "./src/bootstrap",
  +       "./BeritaApp": "./src/bootstrap",
        },
        shared: packageJson.dependencies,
      }),
    ],
  };
  ```

## 3. Penyesuaian dengan module `berita`

- Ubah file **`berita/public/index.html`**

  ```diff
    <div
  -   id="_example-root"
  +   id="_berita-root"
      className="module-wrapper"></div>
  ```

- Ubah file **`berita/src/bootstrap.js`**

  ```diff
  if (process.env.NODE_ENV === "development") {
  - const devRoot = document.querySelector("#_example-root");
  + const devRoot = document.querySelector("#_berita-root");
  ```

## 4. Penyesuaian Routing untuk Module `berita`

- Ubah file **`berita/src/app.tsx`**

  ```diff
  const App: React.FC<AppProps> = ({ history }) => {
    return (
      <HistoryRouter history={history}>
        <Routes>
  -       <Route path="/example-module" element={<Layout />}>
  +       <Route path="/berita" element={<Layout />}>
  -        <Route index element={<h1>This is Example Module</h1>} />
  +        <Route index element={<h1>This is Berita Module</h1>} />
          </Route>
        </Routes>
      </HistoryRouter>
    );
  }
  ```

  Khusus module ini Kita mengubah main route menjadi **`/berita`** yang artinya module ini akan diakses/dimuat jika URL yang di hit oleh user adalah **`/berita/*`** dst

## 5. Konfigurasi Host Module(container)

Untuk bisa dikenali dan dimuat oleh **`Host Module`** kita juga harus mendaftarkan **`Remote Module`** pada konfigurasi **`Host Module`**

- Ubah konfigurasi untuk development environment di file **`container/config/webpack.dev.js`** pada bagian **`ModuleFederationPlugin`**

  ```diff
  new ModuleFederationPlugin({
    name: "container",
    remotes: {
      home: "home@http://localhost:3002/remoteEntry.js",
      auth: "auth@http://localhost:3003/remoteEntry.js",
  +   berita: "berita@http://localhost:3004/remoteEntry.js",
    },
    shared: [packageJson.dependencies],
  }),
  ```

- Ubah konfigurasi untuk production environment di file **`container/config/webpack.prod.js`** pada bagian **`ModuleFederationPlugin`**

  ```diff
  new ModuleFederationPlugin({
    name: "container",
    remotes: {
      home: `home@/home/remoteEntry.js`,
      auth: `auth@/auth/remoteEntry.js`,
  +   berita: `berita@/berita/remoteEntry.js`,
    },
    shared: packageJson.dependencies,
  }),
  ```

## 6. Memuat Module Berita di Host Module(container)

Untuk memuat **`Remote Module(berita)`** pada **`Host Module(container)`** kita memanfaatkan fitur [lazyload module](https://beta.reactjs.org/reference/react/lazy) dari react

- Buatlah **_ModuleFederation component_** untuk memuat module berita **`container/src/components/ModuleFederation/BeritaApp.tsx`**

  ```tsx
  import React, { useRef } from "react";
  import { mount } from "berita/BeritaApp";

  import { useModuleFederation } from "@/hooks/use-module-federation";

  const BeritaApp: React.FC = () => {
    const ref = useRef<HTMLDivElement>(null);
    useModuleFederation({ mount, refElement: ref });

    return <div id="_berita-root" ref={ref} className="module-wrapper" />;
  };

  export default BeritaApp;
  ```

  - **`import React, { useRef } from "react";`** mengimport React dan hooks **`useRef`** dari library react
  - **`import { mount } from "berita/BeritaApp";`** mengimport fungsi **`mount`** dari aplikasi **`BeritaApp`** yang dibuat menggunakan microfrontend
  - **`import { useModuleFederation } from "@/hooks/use-module-federation";`** mengimport custom hook **`useModuleFederation`**
  - **`const BeritaApp: React.FC = () => {`** membuat sebuah functional component bernama **`BeritaApp`** yang akan dimuat dengan fitur **`lazy`** dari react
  - **`const ref = useRef<HTMLDivElement>(null);`** membuat sebuah reference object dengan tipe **`HTMLDivElement`** yang awalnya memiliki nilai **`null`**
  - **`useModuleFederation({ mount, refElement: ref });`** memanggil custom hook useModuleFederation dengan parameter objek yang berisi fungsi **`mount`** dan reference object **`ref`**
  - **`return <div id="_berita-root" ref={ref} className="module-wrapper" />;`** mengembalikan sebuah div element dengan id **`_berita-root`**, reference object **`ref`**, dan class **`module-wrapper`** sebagai tampilan utama dari **`BeritaApp`**
  - **`export default BeritaApp;`** mengekspor **`BeritaApp`** sebagai default export untuk digunakan pada file lain.

- Buatlah **`types`** untuk **`berita/BeritaApp`** agar dikenali oleh typescript dan menghilangkan error pada saat pemanggilan module tersebut.
  Pada file **`container/src/module.d.ts`**

  ```diff
  declare module "home/HomeApp";
  + declare module "berita/BeritaApp";
  ```

- Muat module dan atur routing untuk module berita.
  Pada file **container/src/App.tsx**

  ```tsx
  ...
  const BeritaLazy = lazy(() => import("@/components/ModuleFederation/BeritaApp"));

  const App = () => {
    useTheme();
    return (
      <HistoryRouter history={historyApp}>
        <Routes>
          <Route path="/" element={<RequireAuth><Layout /></RequireAuth>}>
            ...
            <Route
              path="/berita/*"
              element={
                <Suspense fallback={<LoadingModuleIndicator />}>
                  <BeritaLazy />
                </Suspense>
              }
            />
          </Route>
          ...
        </Routes>
      </HistoryRouter>
    );
  };

  export default App;

  ```

## 7. Menambahkan module berita dalam perintah start-dev script pada package.json

ubah file **`package.json`** di root folder pada bagian **`scripts`**

```diff
"start-container": "cd ./container/ && pnpm start",
"start-home": "cd ./home/ && pnpm start",
"start-auth": "cd ./auth/ && pnpm start",
"start-berita": "cd ./berita/ && pnpm start",
+ "start-dev": "concurrently \"pnpm run start-container\"  \"pnpm run start-home\" \"pnpm run start-auth\" \"pnpm run start-berita\""
```

# API Service Berita

Untuk membuat API service ini kita menggunakan fitur **`rtk-query`** dari library **`redux-toolkit`**. **`rtk-query`** memudahkan kita dalam mengatur state untuk request data.

## Setup redux-toolkit

- Pastikan kita berada di root folder dari module **`berita`** lalu jalankan command :

  ```bash
  pnpm i @reduxjs/toolkit
  ```

- Setelah proses diatas selesai lalu kita akan memulai dengan membuat file **`berita/src/state/store.ts`**

  ```ts
  import { configureStore } from "@reduxjs/toolkit";

  export const store = configureStore({
    reducer: {},
    devTools: true,
  });

  export type RootState = ReturnType<typeof store.getState>;

  export type AppDispatch = typeof store.dispatch;
  ```

  - **`import { configureStore } from "@reduxjs/toolkit";`** mengimport fungsi **`configureStore`** dari package **`@reduxjs/toolkit`**
  - **`export const store = configureStore({...})`** membuat sebuah store yang dihasilkan dari **`configureStore`**. Konfigurasi yang diberikan adalah reducer awal kosong **`{}`** dan pengaturan **`devTools: true`** agar kita dapat menggunakan **`Redux DevTools`**.
  - **`export type RootState = ReturnType<typeof store.getState>;`** membuat sebuah tipe bernama **`RootState`** yang mengacu pada tipe balikan fungsi **`getState`** dari store.
  - **`export type AppDispatch = typeof store.dispatch;`** membuat sebuah tipe bernama **`AppDispatch`** yang mengacu pada tipe data dari fungsi **`dispatch`** pada store. **`typeof`** digunakan untuk mendapatkan tipe data dari fungsi **`dispatch`** pada store.

- Lalu kita akan mendaftarkan **`store`** ke dalam aplikasi agar bisa digunakan, dengan cara mengubah file **`berita/src/bootstrap.js`** :

  ```diff
  ...
  + import { Provider } from "react-redux";
  + import { store } from "./state/store";

  const mount = (el, { onNavigate, defaultHistory, initialPath, authSuccess }) => {
  ...
  -  root.render(<App history={history} authSuccess={authSuccess} />);
  +  root.render(
  +   <Provider store={store}>
        <App history={history} authSuccess={authSuccess} />
  +   </Provider>
  +  );
  ...
  }
  ```

## Service Berita

Setelah file **`store`** dibuat, selanjutnya kita akan membuat service berita, namun sebelum kita membuat service berita kita akan membuat **`baseQuery`**.

- **`baseQuery`** pada **`rtk-query`** adalah konfigurasi dasar atau template untuk memanggil endpoint API. Konfigurasi **`baseQuery`** akan menentukan bagaimana **`rtk-query`** akan melakukan request ke API dan memanipulasi hasil respon. Beberapa konfigurasi **`baseQuery`** antara lain URL endpoint, jenis method HTTP yang digunakan, manipulasi data request dan response, header, dan lain-lain. **`baseQuery`** dapat digunakan untuk mengkonfigurasi **`rtk-query`** agar dapat bekerja dengan API yang berbeda-beda dan memberikan pengaturan dasar yang dapat digunakan oleh query-query yang dibuat menggunakan **`rtk-query`**.
  Kita akan membuat file **`berita/src/state/baseQueryWithReauth.ts`** dengan memanfaatkan fungsi **`baseQueryWithReauth`** dari **`@sekjen-dpr/react`** untuk membantu implementasi flow Autentikasi yang dijelaskan sebelumnya

  ```ts
  import { baseQueryWithReauth } from "@sekjen-dpr/react";

  const baseQueryInit = baseQueryWithReauth({
    baseUrl: process.env.API_URL ?? "",
    clientSecret: process.env.clientSecret ?? "",
    clientId: process.env.clientId ?? "",
  });

  export default baseQueryInit;
  ```

  - **`import { baseQueryWithReauth } from "@sekjen-dpr/react";`** mengimport **`baseQueryWithReauth`** dari package **`@sekjen-dpr/react`**.
  - **`const baseQueryInit = baseQueryWithReauth({...})`** membuat variabel baseQueryInit yang isinya adalah fungsi **`baseQueryWithReauth()`** yang diisi dengan objek konfigurasi sebagai parameternya.
  - **`baseUrl: process.env.API_URL ?? ""`** konfigurasi untuk baseUrl yang diambil dari variabel **`API_URL`** pada file **`.env`** atau string kosong jika tidak ada.
  - **`clientSecret: process.env.clientSecret ?? "":`** konfigurasi untuk clientSecret yang diambil dari variabel **`clientSecret`** pada file **`.env`** atau string kosong jika tidak ada.
  - **`clientId: process.env.clientId ?? ""`** konfigurasi untuk clientId yang diambil dari variabel clientId pada file .env atau string kosong jika tidak ada.
  - **`export default baseQueryInit;`** mengekspor variabel **`baseQueryInit`** sebagai default export dari file ini.

- Setelah **`baseQuery`** dibuat kita akan membuat **`interface`** untuk berita yang berguna sebagai kerangka dari **`Berita`**. Buat file pada **`berita/src/types/Berita.ts`**

  ```ts
  export interface BeritaItem {
    id: number | string;
    title: string;
    description: string;
  }
  ```

- Setelah **`interface Berita`** dibuat, selanjutnya kita akan membuat Service Berita. Buat file pada **`berita/state/berita/service.ts`**

  ```ts
  import baseQueryWithReauth from "../baseQueryWithReauth";
  import { createApi } from "@reduxjs/toolkit/query/react";

  export const brBeritaService = createApi({
    reducerPath: "brBeritaService",
    baseQuery: baseQueryWithReauth,
  });
  ```

  - **`import baseQueryWithReauth from "../baseQueryWithReauth";`**: Import file **`baseQueryWithReauth`** yang berisi konfigurasi **`baseQuery`** RTK Query.
  - **`createApi({})`**: Membuat instance service menggunakan createApi dengan argument object berisi:
    - **`reducerPath: "brBeritaService"`**: Nama reducer untuk service ini.
    - **`baseQuery: baseQueryWithReauth`**: Konfigurasi **`baseQuery`** yang digunakan untuk melakukan HTTP request. Konfigurasi ini diambil dari **`baseQueryWithReauth`** yang di-import sebelumnya.
  - **`export const brBeritaService = createApi({})`**: Export instance service yang sudah dibuat dengan nama **`brBeritaService`**. Instance service ini nantinya bisa digunakan untuk melakukan HTTP request terhadap API.

- Lalu kita akan membuat **`endpoint`** untuk request **`list berita`**.

  ```ts
  import { BeritaItem } from "@/types/Berita";
  import { queryMaker, GQLTableQueryMaker, Response, DefaultListParameters, GQLQueryMaker } from "@sekjen-dpr/react";

  type GetBeritaParameters = DefaultListParameters;

  export const brBeritaService = createApi({
    ...
    endpoints: (builder) => ({
      getDaftarBerita: builder.query<Response<BeritaItem[]>, GetBeritaParameters>({
        query: ({ sort, search, ...params }) =>
          queryMaker({
            body: GQLTableQueryMaker({
              field: "news",
              query: `
              data{
                id
                title
                description
              }
              `,
              variables: [
                {
                  queryArgType: "QueryNewsWhereWhereConditions",
                  variableName: "where",
                  variableType: "where",
                  variableValueWhere: {
                    AND: search,
                  },
                },
                ...(sort
                  ? [
                      {
                        queryArgType: "[QueryNewsOrderByOrderByClause!]",
                        variableName: "orderBy",
                        variableValue: sort,
                      },
                    ]
                  : []),
              ],
              ...params,
            }),
          }),
        transformResponse: (response: { data: { news: Response<BeritaItem[]> } }) => response.data.news,
      }),
    }),
  });
  ```

  - **`import { BeritaItem } from "@/types/Berita";`**: Mengimpor tipe **`BeritaItem`** dari file **`Berita.ts`** yang terletak di dalam direktori **`@/types/`**.
  - **`type GetBeritaParameters = DefaultListParameters;`**: Membuat tipe **`GetBeritaParameters`** yang merupakan turunan dari **`DefaultListParameters`**.
  - **`import { queryMaker, GQLTableQueryMaker, Response, DefaultListParameters, GQLQueryMaker } from "@sekjen-dpr/react";`**: Mengimpor beberapa fungsi dan tipe dari package **`@sekjen-dpr/react`**.
    - **`queryMaker`** : Fungsi helper untuk membuat query dalam service
    - **`GQLTableQueryMaker`** : Fungsi helper untuk mempermudah membuat **`Graphql`** query untuk list/table.
    - **`GQLQueryMaker`** : Fungsi helper untuk mempermudah membuat **`Graphql`** query.
    - **`Response`** : Interface Response dari request.
    - **`DefaultListParameters`** : Default Interface untuk List.
  - **`const brBeritaService = createApi({ ... })`**: Membuat instance brBeritaService dari createApi dengan konfigurasi yang diberikan.
  - **`getDaftarBerita: builder.query<...>({ ... })`**: Membuat endpoint getDaftarBerita dengan tipe data respons **`Response<BeritaItem[]>`** dan parameter **`GetBeritaParameters`**.
  - **`query: ({ sort, search, ...params }) => queryMaker({...})`**: Membuat query dengan menggunakan fungsi **`queryMaker`** dari **`@sekjen-dpr/react`** yang membungkus query dari **`GQLTableQueryMaker`**. Query yang dihasilkan akan mencari data pada tabel news dan mengambil kolom **`id`**, **`title`**, dan **`description`**.
  - **`variables: [{ ... }]`**: Mendefinisikan variabel-variabel yang digunakan pada query. Pada kode di atas, terdapat variabel **`where`** dan **`orderBy`** yang dibutuhkan oleh **`GQLTableQueryMaker`**.
  - **`transformResponse: (response: { data: { news: Response<BeritaItem[]> } }) => response.data.news`**: Mengubah respons yang diterima sehingga hanya mengembalikan data berita yang diperoleh dari server.
  - Lalu kita akan export query endpoint yang di jelaskan diatas :

    ```ts
    export const { useGetDaftarBeritaQuery } = brBeritaService;
    ```

    - **`useGetDaftarBeritaQuery`** adalah sebuah hook yang dihasilkan oleh **`createApi`** diatas.
    - Hook ini memungkinkan kita untuk melakukan query ke endpoint **`getDaftarBerita`** yang sudah didefinisikan sebelumnya di dalam objek **`endpoints`** dari **`brBeritaService`**.
    - Hook ini mengembalikan sebuah objek yang berisi informasi terkait hasil query, seperti **`isLoading`**, **`isFetching`**, **`data`**, **`error`**, dan lain-lain.
    - Dalam konteks kode ini, hook ini akan digunakan di dalam komponen React untuk melakukan query data daftar berita dan mengakses hasil query tersebut.

- Setelah itu kita akan membuat endpoint untuk mengambil detail berita.

  ```ts
  export const brBeritaService = createApi({
    ...
    endpoints: (builder) => ({
      ...
      getBeritaById: builder.query<BeritaItem, string | number>({
        query: (id) =>
          queryMaker({
            body: GQLQueryMaker({
              field: "newsById",
              query: `
                id
                title
                description
              `,
              variables: [
                {
                  variableName: "id",
                  variableValue: id,
                  queryArgType: "ID",
                },
              ],
            }),
          }),
        transformResponse: (response: { data: { newsById: BeritaItem } }) => response.data.newsById,
      }),
    })
  })
  ```

  - **`getBeritaById: builder.query<BeritaItem, string | number>({...})`**: mendefinisikan sebuah query endpoint dengan nama **`getBeritaById`** yang akan mengambil data berita sesuai dengan id yang diberikan sebagai parameter.
  - Lalu kita akan export query endpoint diatas :

    ```diff
    export const {
      useGetDaftarBeritaQuery,
    + useGetBeritaByIdQuery
    } = brBeritaService;
    ```

- Setelah itu kita akan membuat endpoint **`create`** untuk berita.

  - Sebelum membuat endpoint **`create`**, kita akan membuat interface **`BeritaFormField`** untuk field-field apa saja yang dibutuhkan, buatlah file pada **`berita/types/Berita.ts`**

    ```ts
    ...
    export interface BeritaFormField {
      title: string;
      description: string;
    }
    ```

  - Setelah membuat interface lalu kita akan membuat endpoint **`create`**.

    ```ts
    import { BeritaFormField, BeritaItem } from "@/types/Berita";
    ...

    export const brBeritaService = createApi({
      ...
      endpoints: (builder) => ({
        ...
        createBerita: builder.mutation<Response<BeritaItem>, BeritaFormField>({
          query: (params) =>
            queryMaker({
              body: GQLQueryMaker({
                queryType: "mutation",
                field: "createNews",
                query: `
                  id
                  title
                  description
                `,
                variables: [
                  {
                    queryArgType: "NewsInput!",
                    variableName: "input",
                    variableValue: params,
                  },
                ],
              }),
            }),
        }),
      })
    })
    ...
    ```

    - **`builder.mutation`**: method dari RTK Query yang digunakan untuk membuat mutation pada API.
    - **`query`** : fungsi yang berfungsi untuk mengambil data berita dengan parameter **`BeritaFormField`**.
    - **`queryType`** : tipe query pada GraphQL, dalam hal ini adalah **`mutation`**.
    - **`field`** : field yang dipanggil pada GraphQL untuk memanggil data berita.
    - **`variables`** : variabel yang dibutuhkan untuk memanggil data berita pada GraphQL, dalam hal ini adalah input yang berisi **`BeritaFormField`**.
    - **`transformResponse`** : fungsi yang berfungsi untuk mengubah hasil response dari API menjadi bentuk yang dapat digunakan oleh aplikasi.
    - **`Response<BeritaItem>`** : tipe response API, dalam hal ini adalah BeritaItem yang berisi field **`id`**, **`title`**, dan **`description`**.

  - Lalu kita akan export query endpoint diatas :

    ```diff
    export const {
      useGetDaftarBeritaQuery,
      useGetBeritaByIdQuery,
    + useCreateBeritaMutation,
    } = brBeritaService;
    ```

- Setelah itu kita akan membuat endpoint **`update`** untuk berita.

  ```ts
  updateBerita: builder.mutation<Response<BeritaItem>, BeritaFormField & { id: string | number }>({
    query: ({ id, ...params }) =>
      queryMaker({
        body: GQLQueryMaker({
          queryType: "mutation",
          field: "updateNews",
          query: `
            id
            title
            description
          `,
          variables: [
            {
              variableName: "id",
              variableValue: id,
              queryArgType: "ID!",
            },
            {
              queryArgType: "NewsInput!",
              variableName: "input",
              variableValue: params,
            },
          ],
        }),
      }),
  }),
  ```

  - **`updateBerita`** hampir sama parameternya dengan **`createBerita`** yang berbeda hanya ada parameter tambahan **`id`**
  - Lalu kita akan export query endpoint diatas :

    ```diff
    export const {
      useGetDaftarBeritaQuery,
      useGetBeritaByIdQuery,
      useCreateBeritaMutation,
    + useUpdateBeritaMutation,
    } = brBeritaService;
    ```

- Setelah itu kita akan membuat endpoint **`delete`** untuk berita.

  ```ts
  deleteBerita: builder.mutation<Response<BeritaItem>, string | number>({
      query: (beritaId) =>
      queryMaker({
        body: GQLQueryMaker({
          queryType: "mutation",
          field: "deleteNews",
          query: `
            id
            title
            description
          `,
          variables: [
            {
              queryArgType: "ID!",
              variableName: "id",
              variableValue: beritaId,
            },
          ],
        }),
      }),
  }),
  ```

  - Lalu kita akan export query endpoint diatas :

    ```diff
    export const {
      useGetDaftarBeritaQuery,
      useGetBeritaByIdQuery,
      useCreateBeritaMutation,
      useUpdateBeritaMutation,
    + useDeleteBeritaMutation,
    } = brBeritaService;
    ```

## Tambahkan service berita kedalam store

Setelah service Berita sudah dibuat, kita akan menambahkan kedalam store. **`RTKQ service`** membuat **`"slice reducer"`** yang harus dimasukan ke dalam root reducer Redux, dan custom middleware untuk menangani data fetching. Keduanya perlu ditambahkan ke dalam toko Redux.

file **`berita/src/state/store.ts`**

```diff
import { configureStore } from "@reduxjs/toolkit";

+ const services = [brBeritaService] as const;

+ const servicesReducer = {
+   [brBeritaService.reducerPath]: brBeritaService.reducer,
+ };

export const store = configureStore({
  reducer: {
+   ...servicesReducer,
  },
  devTools: true,
+ middleware: (getDefaultMiddleware) =>
+    getDefaultMiddleware().concat([...services.map((service) => service.middleware)]),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
```

## Hasil Akhir Dari file **`berita/src/state/store.ts`**

```ts
import { configureStore } from "@reduxjs/toolkit";
import { brBeritaService } from "./berita/service";

const services = [brBeritaService] as const;

const servicesReducer = {
  [brBeritaService.reducerPath]: brBeritaService.reducer,
};

export const store = configureStore({
  reducer: {
    ...servicesReducer,
  },
  devTools: true,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      ...services.map((service) => service.middleware),
    ]),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
```

## Hasil Akhir Dari file **`berita/src/state/baseQueryWithReauth.ts`**

```ts
import { baseQueryWithReauth } from "@sekjen-dpr/react";

const baseQueryInit = baseQueryWithReauth({
  baseUrl: process.env.API_URL ?? "",
  clientSecret: process.env.clientSecret ?? "",
  clientId: process.env.clientId ?? "",
});

export default baseQueryInit;
```

## Hasil Akhir Dari file **`berita/src/state/berita/service.ts`**

```ts
import { BeritaFormField, BeritaItem } from "@/types/Berita";
import { createApi } from "@reduxjs/toolkit/query/react";
import {
  queryMaker,
  GQLTableQueryMaker,
  Response,
  DefaultListParameters,
  GQLQueryMaker,
} from "@sekjen-dpr/react";
import baseQueryWithReauth from "../baseQueryWithReauth";

type GetBeritaParameters = DefaultListParameters;

export const brBeritaService = createApi({
  reducerPath: "brBeritaService",
  baseQuery: baseQueryWithReauth,
  endpoints: (builder) => ({
    getDaftarBerita: builder.query<Response<BeritaItem[]>, GetBeritaParameters>(
      {
        query: ({ sort, search, ...params }) =>
          queryMaker({
            body: GQLTableQueryMaker({
              field: "news",
              query: `
              data{
                id
                title
                description
              }
              `,
              variables: [
                {
                  queryArgType: "QueryNewsWhereWhereConditions",
                  variableName: "where",
                  variableType: "where",
                  variableValueWhere: {
                    AND: search,
                  },
                },
                ...(sort
                  ? [
                      {
                        queryArgType: "[QueryNewsOrderByOrderByClause!]",
                        variableName: "orderBy",
                        variableValue: sort,
                      },
                    ]
                  : []),
              ],
              ...params,
            }),
          }),
        transformResponse: (response: {
          data: { news: Response<BeritaItem[]> };
        }) => response.data.news,
      }
    ),
    getBeritaById: builder.query<BeritaItem, string | number>({
      query: (id) =>
        queryMaker({
          body: GQLQueryMaker({
            field: "newsById",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                variableName: "id",
                variableValue: id,
                queryArgType: "ID",
              },
            ],
          }),
        }),
      transformResponse: (response: { data: { newsById: BeritaItem } }) =>
        response.data.newsById,
    }),
    createBerita: builder.mutation<Response<BeritaItem>, BeritaFormField>({
      query: (params) =>
        queryMaker({
          body: GQLQueryMaker({
            queryType: "mutation",
            field: "createNews",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                queryArgType: "NewsInput!",
                variableName: "input",
                variableValue: params,
              },
            ],
          }),
        }),
    }),
    updateBerita: builder.mutation<
      Response<BeritaItem>,
      BeritaFormField & { id: string | number }
    >({
      query: ({ id, ...params }) =>
        queryMaker({
          body: GQLQueryMaker({
            queryType: "mutation",
            field: "updateNews",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                variableName: "id",
                variableValue: id,
                queryArgType: "ID!",
              },
              {
                queryArgType: "NewsInput!",
                variableName: "input",
                variableValue: params,
              },
            ],
          }),
        }),
    }),
    deleteBerita: builder.mutation<Response<BeritaItem>, string | number>({
      query: (beritaId) =>
        queryMaker({
          body: GQLQueryMaker({
            queryType: "mutation",
            field: "deleteNews",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                queryArgType: "ID!",
                variableName: "id",
                variableValue: beritaId,
              },
            ],
          }),
        }),
    }),
  }),
});
export const {
  useGetDaftarBeritaQuery,
  useGetBeritaByIdQuery,
  useCreateBeritaMutation,
  useUpdateBeritaMutation,
  useDeleteBeritaMutation,
} = brBeritaService;
```

# List Berita

Pertama kita harus menginstall library **`react-table`**, pastikan anda di root folder **`berita/`** dan jalankan command :

```bash
pnpm i react-table
pnpm i @types/react-table -D
```

## Membuat Table Component Berita

Untuk Table Component Berita kita harus membuat 2 component:

- TableComponent
- TableColumnComponent

### TableComponent untuk Berita

Kita harus membuat file pada folder **`berita/src/components/Tables`** :

- **`berita/src/components/Tables/BeritaTableComponent.tsx`**

  ```tsx
  import { useGetDaftarBeritaQuery } from "@/state/berita/service";
  import { BeritaItem } from "@/types/Berita";
  import { Table, useTableDpr } from "@sekjen-dpr/react";
  import React from "react";

  const BeritaTableComponent = () => {
    const { columnsRow, onPageChange, data, tableState } = useTableDpr<BeritaItem>({
      query: useGetDaftarBeritaQuery,
      tableColumn: ...,
    });

    return (
      <Table<BeritaItem>
        data={data?.data || []}
        isLoading={!!tableState.isLoading}
        pagination={{
          ...tableState,
          onPageChange,
        }}
        tableColumns={columnsRow}
      />
    );
  };

  export default BeritaTableComponent;
  ```

  - **`import { useGetDaftarBeritaQuery } from "@/state/berita/service";`**: mengimport hook **`useGetDaftarBeritaQuery`** dari file **`service.ts`** yang berada di dalam folder **`berita`** pada direktori **`state`**.
  - **`import { BeritaItem } from "@/types/Berita";`**: mengimport interface **`BeritaItem`** dari file **`Berita.ts`** yang berada di dalam folder **`types`** pada direktori **`src`**.
  - **`import { Table, useTableDpr } from "@sekjen-dpr/react";`**: mengimport komponen **`Table`** dan hook **`useTableDpr`** dari library **`@sekjen-dpr/react`**.
  - **`const BeritaTableComponent = () => {...}`**: mendefinisikan functional component BeritaTableComponent yang akan menampilkan data berita dalam bentuk tabel.
  - **`const { columnsRow, onPageChange, data, tableState } = useTableDpr<BeritaItem>({...})`**: menggunakan hook **`useTableDpr`** dengan generic type **`BeritaItem`** untuk mengatur state dan logic pada tabel, dengan memasukkan props **`query`** dan **`tableColumn`**.
  - **`data?.data || []`**: mengakses property **`data`** pada **`data`** dan memberikan default value **`[]`** jika **`data`** tidak ada (undefined/null).
  - **`isLoading={!!tableState.isLoading}`**: mengatur status loading pada tabel dengan mengecek apakah **`isLoading`** pada **`tableState`** bernilai truthy (not null/undefined).
  - **`pagination={{...}}`**: mengatur pagination pada tabel dengan memasukkan props **`tableState`** dan **`onPageChange`**.
  - **`tableColumns={columnsRow}`**: mengatur kolom-kolom pada tabel dengan memasukkan nilai dari **`columnsRow`**.
  - **`export default BeritaTableComponent;`**: mengekspor komponen **`BeritaTableComponent`** sebagai default export dari file ini.

- Untuk mengisi key **`tableColumn`** dalam hook **`useTableDpr`**, untuk membuat column-column pada table.
  **`berita/src/components/Tables/BeritaTableColumn.tsx`**

  ```tsx
  import React from "react";
  import { Button, ColumnsInterface, TableHeader } from "@sekjen-dpr/react";
  import { Column } from "react-table";
  import { BeritaItem } from "@/types/Berita";
  import { Link } from "react-router-dom";

  const BeritaTableColumn = (
    props: ColumnsInterface<BeritaItem>
  ): ReadonlyArray<Column<BeritaItem>> => {
    const {
      onTableSort,
      tableSortState,
      onTableSearch,
      tableSearchStates,
      isLoading,
    } = props;
    return [];
  };
  ```

  - **`import React from "react";`**: mengimpor modul React dari package react
  - **`import { Button, ColumnsInterface, TableHeader  } from "@sekjen-dpr/react";`**: mengimpor component **`Button`** ,**`ColumnsInterface`** dan **`TableHeader`** dari package **`@sekjen-dpr/react`**
  - **`import { Column } from "react-table";`**: mengimpor modul **`Column`** dari package **`react-table`**
  - **`import { BeritaItem } from "@/types/Berita";`**: mengimpor modul **`BeritaItem`** dari file dengan path **`@/types/Berita`**
  - **`import { Link } from "react-router-dom";`**: mengimpor component **`Link`** dari package **`react-router-dom`**
  - **`const BeritaTableColumn = (props: ColumnsInterface<BeritaItem>)*: ReadonlyArray<Column<BeritaItem>> => {`**: Mendefinisikan sebuah fungsi dengan nama **`BeritaTableColumn`** yang mengambil satu argumen **`props`** dengan tipe **`ColumnsInterface<BeritaItem>`** dan mengembalikan sebuah array of **`Column<BeritaItem>`**
  - **`const { onTableSort, tableSortState, onTableSearch, tableSearchStates, isLoading } = props;`**: mendefinisikan beberapa variabel yang diambil dari properti props

    - **`onTableSort`** : fungsi event ketika tabel diurutkan
    - **`tableSortState`** : state dari tabel yang diurutkan
    - **`onTableSearch`** : fungsi event ketika tabel dicari
    - **`tableSearchStates`** : state dari tabel yang dicari
    - **`isLoading`** : state dari tabel yang sedang loading

  - Membuat Column **`ID`** dan tambahkan ke dalam array **`columnsRow`** yang akan dikembalikan oleh fungsi **`BeritaTableColumn`**

    ```tsx
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          tableProps={props}
          isLoading={isLoading}
          sortable={{
            onSort: onTableSort,
            sortState: tableSortState,
          }}
          searchable={{
            key: "id",
            onSearch: onTableSearch,
            tableSearchStates,
          }}
          title="Id"
          className="w-70px text-center"
        />
      ),
      id: "id",
      Cell: (props: any) => <div>{props.row.original.id}</div>,
    }
    ```

    - **`Header: (props) => (...`** : membuat sebuah kolom dengan nama **`ID`** yang akan menampilkan header dari kolom tersebut
    - **`<TableHeader<BeritaItem>`** : memanggil komponen **`TableHeader`** dengan generic type **`BeritaItem`**
    - **`tableProps={props}`** : memasukkan props yang diterima oleh fungsi **`BeritaTableColumn`** ke dalam props **`tableProps`** dari komponen **`TableHeader`**
    - **`isLoading={isLoading}`** : memasukkan nilai dari variabel **`isLoading`** ke dalam props **`isLoading`** dari komponen **`TableHeader`**. Yang digunakan untuk disable input pada header tabel ketika tabel sedang loading
    - **`sortable={{...}}`** : memasukkan props **`onSort`** dan **`sortState`** ke dalam props **`sortable`** dari komponen **`TableHeader`**. Yang digunakan untuk mengurutkan tabel berdasarkan kolom yang dipilih
    - **`searchable={{...}}`** : memasukkan props **`key`**, **`onSearch`**, dan **`tableSearchStates`** ke dalam props **`searchable`** dari komponen **`TableHeader`**. Yang digunakan untuk mencari data pada tabel berdasarkan kolom yang dipilih. dimana **`key`** digunakan untuk menentukan nama kolom yang akan dicari ke graphql service
    - **`title="Id"`** : memasukkan nilai **`Id`** ke dalam props **`title`** dari komponen **`TableHeader`**. Yang digunakan untuk menampilkan judul pada header tabel
    - **`className="w-70px text-center"`** : memasukkan nilai **`w-70px text-center`** ke dalam props **`className`** dari komponen **`TableHeader`**. Yang digunakan untuk menambahkan class pada header tabel
    - **`id: "id"`** : memasukkan nilai **`id`** ke dalam props **`id`** dari kolom **`ID`**. Yang digunakan untuk menentukan nama kolom pada tabel
    - **`Cell: (props: any) => <div>{props.row.original.id}</div>`** : memasukkan nilai **`props.row.original.id`** ke dalam props **`Cell`** dari kolom **`ID`**. Yang digunakan untuk menampilkan data **`id`** pada kolom ini

  - Membuat Column **`Title`** dan tambahkan ke dalam array **`columnsRow`** yang akan dikembalikan oleh fungsi **`BeritaTableColumn`**

    ```tsx
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          tableProps={props}
          isLoading={isLoading}
          sortable={{
            onSort: onTableSort,
            sortState: tableSortState,
          }}
          searchable={{
            key: "title",
            onSearch: onTableSearch,
            tableSearchStates,
          }}
          title="Judul"
          className="min-w-125px"
        />
      ),
      id: "title",
      Cell: (props: any) => {
        return <span>{props.row.original.title}</span>;
      },
    },
    ```

    - Hal yang sama dengan column sebelumnya. Perbedaannya hanya pada **`id`**, **`Cell`**, **`title`**, dan **`className`**

  - Membuat Column **`Description`** dan tambahkan ke dalam array **`columnsRow`** yang akan dikembalikan oleh fungsi **`BeritaTableColumn`**

    ```tsx
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          tableProps={props}
          isLoading={isLoading}
          searchable={{
            key: "description",
            onSearch: onTableSearch,
            tableSearchStates,
          }}
          title="Deskripsi"
          className="min-w-125px"
        />
      ),
      id: "description",
      Cell: (props: any) => {
        return <span>{props.row.original.description}</span>;
      },
    },
    ```

    - Hal yang sama dengan column sebelumnya. Perbedaannya hanya pada **`id`**, **`Cell`**, **`title`**, **`className`**, dan **`sortable`**

  - Membuat Column **`Action`** dan tambahkan ke dalam array **`columnsRow`** yang akan dikembalikan oleh fungsi **`BeritaTableColumn`**

    ```tsx
    {
      Header: (props) => (
        <TableHeader<BeritaItem> isLoading={isLoading} tableProps={props} title="Action" className="text-end w-100px" />
      ),
      id: "action",
      Cell: (props: any) => (
        <Button color="primary" RenderComponent={Link} className="me-2" to={`edit/${props.row.original.id}`}>
          Edit
        </Button>
      ),
    },
    ```

    - Hal yang sama dengan column sebelumnya. Perbedaannya hanya pada **`id`**, **`Cell`**, **`title`**, dan **`className`**
    - Cell berisi button **`Edit`** yang akan mengarahkan ke halaman **`/berita/edit/:id`** dengan parameter **`:id`** berisi **`id`** dari data yang akan diedit

- Lalu setelah **`BeritaTableColumn`** sudah dibuat maka kita akan memasukan component **`BeritaTableColumn`** kedalam key **`tableColumn`** didalam komponen **`BeritaTableComponent`**

  ```diff
  ...
  + import BeritaTableColumn from "./BeritaTableColumn";

  const BeritaTableComponent = () => {
    const { columnsRow, onPageChange, data, tableState } = useTableDpr<BeritaItem>({
      query: useGetDaftarBeritaQuery,
  +    tableColumn: BeritaTableColumn,
    });
    ...
  };
  ```

- Setelah itu kita akan membuat **`index.ts`** didalam **`berita/src/components/Tables`**

  ```ts
  export { default } from "./BeritaTableComponent";
  ```

  Baris kode tersebut merupakan sebuah sintaks ES6 yang digunakan untuk melakukan export secara default dari sebuah modul pada file **`BeritaTableComponent.tsx`**. Kode tersebut mengambil fungsi **`default`** dari file tersebut dan mengekspornya secara default. Dengan cara ini, file **`BeritaTableComponent.tsx`** dapat diimpor dan digunakan pada file lain dengan lebih mudah menggunakan sintaks **`import BeritaTableComponent from './BeritaTableComponent'`**.

## Page Berita

Kita akan membuat page **`Berita`** yang berisi tabel data **`Berita`** yang sudah kita buat sebelumnya. Di page ini juga akan ada tombol **`Tambah Berita`** yang akan mengarahkan ke halaman **`/berita/tambah`**.

### Membuat Page Berita

- Kita akan membuat file **`Berita.tsx`** didalam folder **`berita/src/pages/Berita`**

  ```tsx
  import React from "react";
  import BeritaTableComponent from "@/components/Tables/BeritaTableComponent";
  import { Card } from "@sekjen-dpr/react";

  const Berita = () => {
    return (
      <Card shadow className="mb-10">
        <Card.Header>
          <h3 className="card-title d-flex align-items-start flex-column">
            <span className="card-label fw-bold fs-3 mb-1">Daftar Berita</span>
          </h3>
        </Card.Header>
        <Card.Body className="p-0">
          <BeritaTableComponent />
        </Card.Body>
      </Card>
    );
  };

  export default Berita;
  ```

  - **`import BeritaTableComponent from "@/components/Tables/BeritaTableComponent";`** : mengimport component **`BeritaTableComponent`** yang sudah kita buat sebelumnya
  - **`import { Card } from "@sekjen-dpr/react";`** : mengimport component **`Card`** dari **`@sekjen-dpr/react`**
  - **`const Berita = () => {...}`** : membuat component **`Berita`** yang akan mengembalikan sebuah **`Card`** yang berisi **`BeritaTableComponent`**
  - **`<Card shadow className="mb-10">`** : membuat **`Card`** dengan properti **`shadow`** dan **`className`** yang bernilai **`mb-10`**
  - **`<Card.Header>`** : membuat **`Card.Header`** yang berisi **`Daftar Berita`**
  - **`<Card.Body className="p-0">`** : membuat **`Card.Body`** dengan properti **`className`** yang bernilai **`p-0`**
  - **`<BeritaTableComponent />`** : memanggil component **`BeritaTableComponent`**

- Lalu kita akan menambahkan button **`Tambah Berita`** di bagian **`Card Header`**

  ```diff
  ...
  - import { Card } from "@sekjen-dpr/react";
  + import { Button, Card } from "@sekjen-dpr/react";
  + import { Link } from "react-router-dom";

  const Berita = () => {
    return (
      <Card shadow className="mb-10">
        <Card.Header>
          ...
  +       <div className="card-toolbar">
  +         <Button color="primary" RenderComponent={Link} to="tambah">
  +           + Tambah Berita
  +         </Button>
  +       </div>
        </Card.Header>
        ...
      </Card>
    );
  };
  ```

  - **`import { Button, Card } from "@sekjen-dpr/react";`** : menambahkan import component **`Button`** dari **`@sekjen-dpr/react`**
  - **`import { Link } from "react-router-dom";`** : menambahkan import component **`Link`** dari **`react-router-dom`**
  - **`<div className="card-toolbar">`** : membuat div dengan properti **`className`** yang bernilai **`card-toolbar`**
  - **`<Button color="primary" RenderComponent={Link} to="tambah">`** : membuat button dengan properti **`color`** yang bernilai **`primary`**, **`RenderComponent`** yang bernilai **`Link`**, dan **`to`** yang bernilai **`tambah`**
  - **`+ Tambah Berita`** : membuat text **`+ Tambah Berita`** didalam button

- Lalu kita akan membuat file **`index.ts`** didalam folder **`berita/src/pages/Berita`**

  ```ts
  export { default } from "./Berita";
  ```

  Baris kode tersebut merupakan sebuah sintaks ES6 yang digunakan untuk melakukan export secara default dari sebuah modul pada file **`Berita.tsx`**. Kode tersebut mengambil fungsi **`default`** dari file tersebut dan mengekspornya secara default. Dengan cara ini, file **`Berita.tsx`** dapat diimpor dan digunakan pada file lain dengan lebih mudah menggunakan sintaks **`import Berita from './Berita'`**.

- Menambahkan Page Berita ke Route di **`berita/src/App.tsx`**

  ```diff
  ...
  + import Berita from "@/pages/Berita";

  const App: React.FC<AppProps> = ({ history }) => {
    return (
      <HistoryRouter history={history}>
        <Routes>
          <Route path="/berita" element={<Layout />}>
  -         <Route index element={<h1>This is Berita Module</h1>} />
  +         <Route index element={<Berita />} />
          </Route>
        </Routes>
      </HistoryRouter>
    );
  };

  export default App;
  ```

  - **`import Berita from "@/pages/Berita";`** : mengimport component **`Berita`** yang sudah kita buat sebelumnya
  - **`<Route index element={<Berita />} />`** : mengganti isi dari path **`index`** **`/berita`** dengan component **`Berita`**

## Hasil Akhir Dari file

### berita/src/App.tsx

```tsx
import React from "react";
import {
  Route,
  Routes,
  unstable_HistoryRouter as HistoryRouter,
} from "react-router-dom";
import { BrowserHistory } from "history";
import Layout from "@/components/Layout";
import Berita from "./pages/Berita";

interface AppProps {
  history: BrowserHistory;
}

const App: React.FC<AppProps> = ({ history }) => {
  return (
    <HistoryRouter history={history}>
      <Routes>
        <Route path="/berita" element={<Layout />}>
          <Route index element={<Berita />} />
        </Route>
      </Routes>
    </HistoryRouter>
  );
};

export default App;
```

### /berita/src/tables/BeritaTableComponent/BeritaTableComponent.tsx

```tsx
import { useGetDaftarBeritaQuery } from "@/state/berita/service";
import { BeritaItem } from "@/types/Berita";
import { Table, useTableDpr } from "@sekjen-dpr/react";
import React from "react";
import BeritaTableColumn from "./BeritaTableColumn";

const BeritaTableComponent = () => {
  const { columnsRow, onPageChange, data, tableState } =
    useTableDpr<BeritaItem>({
      query: useGetDaftarBeritaQuery,
      tableColumn: BeritaTableColumn,
    });

  return (
    <Table<BeritaItem>
      data={data?.data || []}
      isLoading={!!tableState.isLoading}
      pagination={{
        ...tableState,
        onPageChange,
      }}
      tableColumns={columnsRow}
    />
  );
};

export default BeritaTableComponent;
```

### berita/src/tables/BeritaTableComponent/BeritaTableColumn.tsx

```tsx
import React from "react";
import { Button, ColumnsInterface, TableHeader } from "@sekjen-dpr/react";
import { Column } from "react-table";
import { BeritaItem } from "@/types/Berita";
import { Link } from "react-router-dom";

const BeritaTableColumn = (
  props: ColumnsInterface<BeritaItem>
): ReadonlyArray<Column<BeritaItem>> => {
  const {
    onTableSort,
    tableSortState,
    onTableSearch,
    tableSearchStates,
    isLoading,
  } = props;

  return [
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          tableProps={props}
          isLoading={isLoading}
          sortable={{
            onSort: onTableSort,
            sortState: tableSortState,
          }}
          searchable={{
            key: "id",
            onSearch: onTableSearch,
            tableSearchStates,
          }}
          title="Id"
          className="w-70px text-center"
        />
      ),
      id: "id",
      Cell: (props: any) => <div>{props.row.original.id}</div>,
    },
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          tableProps={props}
          isLoading={isLoading}
          sortable={{
            onSort: onTableSort,
            sortState: tableSortState,
          }}
          searchable={{
            key: "title",
            onSearch: onTableSearch,
            tableSearchStates,
          }}
          title="Judul"
          className="min-w-125px"
        />
      ),
      id: "title",
      Cell: (props: any) => {
        return <span>{props.row.original.title}</span>;
      },
    },
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          tableProps={props}
          isLoading={isLoading}
          searchable={{
            key: "description",
            onSearch: onTableSearch,
            tableSearchStates,
          }}
          title="Deskripsi"
          className="min-w-125px"
        />
      ),
      id: "description",
      Cell: (props: any) => {
        return <span>{props.row.original.description}</span>;
      },
    },
    {
      Header: (props) => (
        <TableHeader<BeritaItem>
          isLoading={isLoading}
          tableProps={props}
          title="Action"
          className="text-end w-100px"
        />
      ),
      id: "action",
      Cell: (props: any) => (
        <Button
          color="primary"
          RenderComponent={Link}
          className="me-2"
          to={`edit/${props.row.original.id}`}
        >
          Edit
        </Button>
      ),
    },
  ];
};

export default BeritaTableColumn;
```

### berita/src/BeritaTableComponent/index.ts

```ts
export { default } from "./BeritaTableComponent";
```

### berita/src/pages/Berita/Berita.tsx

```tsx
import React from "react";
import BeritaTableComponent from "@/components/Tables/BeritaTableComponent";
import { Button, Card } from "@sekjen-dpr/react";
import { Link } from "react-router-dom";

const Berita = () => {
  return (
    <Card shadow className="mb-10">
      <Card.Header>
        <h3 className="card-title d-flex align-items-start flex-column">
          <span className="card-label fw-bold fs-3 mb-1">Daftar Berita</span>
        </h3>
        <div className="card-toolbar">
          <Button color="primary" RenderComponent={Link} to="tambah">
            + Tambah Berita
          </Button>
        </div>
      </Card.Header>
      <Card.Body className="p-0">
        <BeritaTableComponent />
      </Card.Body>
    </Card>
  );
};

export default Berita;
```

### berita/src/pages/Berita/index.ts

```ts
export { default } from "./Berita";
```
# Form Berita

Pertama kita harus menginstall library yang berkaitan dengan **`Form`**, pastikan anda di root folder **`berita/`** dan jalankan command :

```bash
pnpm i react-hook-form @hookform/error-message @hookform/resolvers yup@0.32.11
```

- **`react-hook-form`** adalah sebuah library React yang digunakan untuk membuat form dengan mudah dan efisien. Dalam library ini terdapat banyak fitur, seperti validasi form, integrasi dengan banyak jenis input, dan fitur lainnya yang membuat penggunaan form di aplikasi React menjadi lebih mudah dan terstruktur.
- **`@hookform/error-message`** adalah sebuah library yang menyediakan komponen ErrorMessage yang digunakan untuk menampilkan pesan kesalahan saat validasi form yang dilakukan oleh react-hook-form gagal.
- **`@hookform/resolvers`** adalah sebuah library yang menyediakan resolvers untuk react-hook-form. Resolvers ini membantu memvalidasi nilai form dengan menggunakan skema validasi, sehingga mempermudah proses validasi form di dalam aplikasi.
- **`yup`** adalah sebuah library validasi schema untuk JavaScript. Library ini menyediakan API untuk membuat skema validasi yang dapat digunakan untuk memvalidasi data. Yup mendukung berbagai jenis validasi, seperti validasi tipe data, string, angka, dan validasi benar atau salah (boolean). Dalam penggunaannya dengan **`react-hook-form`**, **`yup`** digunakan untuk membuat skema validasi yang kemudian digunakan oleh resolvers dari **`@hookform/resolvers`**.

## Membuat Form Berita

Untuk membuat form Berita kita akan membuat komponen baru bernama **`BeritaForm`**. Buat folder **`BeritaForm`** di dalam folder **`src/components/Forms`** dan file baru didalamnya dengan nama **`BeritaForm.tsx`**. Kemudian tambahkan kode berikut :

```tsx
import React from "react";
import { BeritaFormField, BeritaItem } from "@/types/Berita";
import {
  useCreateBeritaMutation,
  useGetBeritaByIdQuery,
  useUpdateBeritaMutation,
} from "@/state/berita/service";
import yup, { SchemaOf } from "yup";
import { Card, useCustomHookForm } from "@sekjen-dpr/react";

const schemaForm: SchemaOf<BeritaFormField> = yup.object({
  title: yup.string().required("Judul tidak boleh kosong"),
  description: yup.string().required("Deskripsi tidak boleh kosong"),
});

const resetValue: BeritaFormField = {
  title: "",
  description: "",
};

interface BeritaFormProps {
  id?: string;
}

const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {
  const isEditMode = !!id;
  const navigate = useNavigate();
  const { data, isSuccess, isLoading, isFetching } = useGetBeritaByIdQuery(
    id ?? "",
    { skip: !isEditMode }
  );
  const [createBerita] = useCreateBeritaMutation();
  const [updateBerita] = useUpdateBeritaMutation();
  const {
    isFormLoading,
    classNames: { additionalClassNameLabel, classNameForm },
    renderError,
    isFormDisabled,
    shouldUpdateDefaultValueWithData,
    setDefaultValue,
    useForm: { register, handleSubmit, errors, getValues },
  } = useCustomHookForm<BeritaFormField, BeritaItem>({
    schemaForm,
    isFetching,
    isLoading,
    isSuccess,
    data,
    isEditMode,
    resetValue,
  });
  const onSubmit = handleSubmit(async () => {
    const values = getValues();
    if (isEditMode) {
      await updateBerita({ ...values, id: id ?? "" }).unwrap();
    } else {
      await createBerita(values).unwrap();
    }
    navigate("/berita");
  });
  return (
    <>
      <Card shadow className="mb-10 w-800px mx-auto mw-100">
        <Card.Header>
          <h3 className="card-title d-flex align-items-start flex-column">
            <span className="card-label fw-bold fs-3 mb-1">
              {isEditMode ? "Ubah" : "Tambah"} Berita
            </span>
          </h3>
        </Card.Header>
        <Card.Body>
          <form onSubmit={onSubmit}>{/* FORM AKAN ADA DI SINI */}</form>
        </Card.Body>
      </Card>
    </>
  );
};

export default BeritaForm;
```

- **`import { BeritaFormField, BeritaItem } from "@/types/Berita";`** : Import tipe data **`BeritaFormField`** dan **`BeritaItem`** dari file **`/types/Berita.ts`**.
- **`import { useCreateBeritaMutation, useGetBeritaByIdQuery, useUpdateBeritaMutation } from "@/state/berita/service";`** : **`useCreateBeritaMutation`**, **`useGetBeritaByIdQuery`**, dan **`useUpdateBeritaMutation`** adalah hook yang dibuat otomatis oleh **`rtk-query`** dari service yang kita buat sebelumnya.
- **`import yup, { SchemaOf } from "yup";`** : Import **`yup`** dan **`SchemaOf`** dari library **`yup`**.
- **`import { Card, useCustomHookForm } from "@sekjen-dpr/react";`** : Import **`Card`** dan **`useCustomHookForm`** dari library **`@sekjen-dpr/react`**.
- **`const schemaForm: SchemaOf<BeritaFormField> = yup.object`** : Buat skema validasi untuk form berita dengan menggunakan **`yup`**. Skema validasi ini akan digunakan oleh **`useCustomHookForm`** untuk memvalidasi nilai form.
  - **`title: yup.string().required("Judul tidak boleh kosong"),`** : Buat validasi untuk field **`title`**. Field ini harus diisi dan tidak boleh kosong.
  - **`description: yup.string().required("Deskripsi tidak boleh kosong"),`** : Buat validasi untuk field **`description`**. Field ini harus diisi dan tidak boleh kosong.
- **`const resetValue: BeritaFormField`** : Buat nilai default untuk form berita.
  - **`title: "",`** : Nilai default untuk field **`title`** adalah string kosong.
  - **`description: "",`** : Nilai default untuk field **`description`** adalah string kosong.
- **`interface BeritaFormProps`** : Buat interface untuk props komponen **`BeritaForm`**.
  - **`id?: string;`** : Buat properti **`id`** yang bertipe data **`string`**. Properti ini bersifat opsional.
- **`const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {`** : Buat komponen **`BeritaForm`** dengan menggunakan **`React.FC`**. Komponen ini menerima props dengan tipe data **`BeritaFormProps`**.
- **`const isEditMode = !!id;`** : Buat variabel **`isEditMode`** dengan nilai **`true`** jika properti **`id`** tidak kosong.
- **`const navigate = useNavigate();`** : Buat variabel **`navigate`** dengan menggunakan hook **`useNavigate`**.
- **`const { data, isSuccess, isLoading, isFetching } = useGetBeritaByIdQuery(id ?? "", { skip: !isEditMode });`** : digunakan untuk memanggil hook useGetBeritaByIdQuery yang akan mengambil data berita berdasarkan id jika id tidak bernilai **`null`** atau **`undefined`**.
- **`const [createBerita] = useCreateBeritaMutation();`** : digunakan untuk memanggil hook useCreateBeritaMutation yang akan digunakan untuk membuat data berita baru.
- **`const [updateBerita] = useUpdateBeritaMutation();`** : digunakan untuk memanggil hook useUpdateBeritaMutation yang akan digunakan untuk update data berita.
- **`const {`** : Buat variabel **`isFormLoading`**, **`classNames`**, **`renderError`**, **`shouldUpdateDefaultValueWithData`**, **`setDefaultValue`**, **`isFormDisabled`**, dan **`useForm`** dengan menggunakan **`useCustomHookForm`**.
  - **`isFormLoading`** : digunakan untuk mengetahui apakah form sedang dalam proses loading atau tidak.
  - **`classNames`** : digunakan untuk menambahkan class pada form.
    - **`additionalClassNameLabel`** : digunakan untuk menambahkan class pada label form.
    - **`classNameForm`** : digunakan untuk menambahkan class pada form.
  - **`renderError`** : digunakan untuk menampilkan error pada form.
  - **`isFormDisabled`** : digunakan untuk menentukan apakah form akan dinonaktifkan atau tidak.
  - **`shouldUpdateDefaultValueWithData`** : digunakan untuk menentukan waktu dimana form harus di ubah dengan data baru, jika ada datanya.
  - **`setDefaultValue`** : digunakan untuk mengubah nilai default form.
  - **`useForm`** : digunakan untuk mengakses fungsi-fungsi yang ada pada **`react-hook-form`**.
    - **`register`** : digunakan untuk mendaftarkan field pada form.
    - **`handleSubmit`** : digunakan untuk menangani submit form.
    - **`errors`** : digunakan untuk menampilkan error pada form.
    - **`getValues`** : digunakan untuk mengambil nilai dari form.
- **`const onSubmit = handleSubmit(async () => { ... }`** : digunakan untuk menentukan action yang akan diambil saat form input disubmit. Lalu jika
- **`return ( ... )`** : digunakan untuk mengembalikan nilai dari komponen **`BeritaForm`**.
- **`<span className="card-label fw-bold fs-3 mb-1">{isEditMode ? "Ubah" : "Tambah"} Berita</span>`** : digunakan untuk menampilkan judul form berita.
- **`<form onSubmit={onSubmit}>`** : digunakan untuk membuat form input berita.

## Membuat Form Input Berita

Kita akan membuat input form dan menempatkannya pada bagian **`/* FORM AKAN ADA DI SINI */`** yang ada pada file **`/components/Forms/BeritaForm/BeritaForm.tsx`**.

- Input Form **`judul`** :

  ```tsx
  <TextInput
    disabled={isFormDisabled}
    id="judul"
    label="Judul Berita"
    className={classNameForm}
    hasError={!!errors.title}
    placeholder="Masukan Judul Berita"
    additionalClassNameLabel={additionalClassNameLabel}
    {...register("title")}
    renderErrorMessage={renderError("title")}
  />
  ```

  - **`<TextInput />`** : digunakan untuk membuat input form dengan menggunakan komponen **`TextInput`** dari **`@sekjen-dpr/react`**.
  - **`disabled={isFormDisabled}`** : digunakan untuk menentukan apakah form akan dinonaktifkan atau tidak.
  - **`id="judul"`** : digunakan untuk menentukan id dari input form.
  - **`label="Judul Berita"`** : digunakan untuk menentukan label dari input form.
  - **`className={classNameForm}`** : digunakan untuk menambahkan class pada input form.
  - **`hasError={!!errors.title}`** : digunakan untuk menentukan apakah input form memiliki error atau tidak.
  - **`placeholder="Masukan Judul Berita"`** : digunakan untuk menentukan placeholder dari input form.
  - **`additionalClassNameLabel={additionalClassNameLabel}`** : digunakan untuk menambahkan class pada label input form.
  - **`{...register("title")}`** : digunakan untuk mendaftarkan field **`title`** pada form.
  - **`renderErrorMessage={renderError("title")}`** : digunakan untuk menampilkan error pada input form.

- Input Form **`deskripsi`** :
  Karena kita deskripsi berita bisa berisi banyak karakter, maka kita akan menggunakan **`TextareaInput`** untuk input form deskripsi berita.

  - Kita akan menambahkan **`TextareaInput`** dari **`@sekjen-dpr/react`**

    ```diff
    - import { Card, TextInput, useCustomHookForm } from "@sekjen-dpr/react";
    + import { Card, TextInput, TextareaInput, useCustomHookForm } from "@sekjen-dpr/react";
    ```

  - Lalu Kita akan menambahkan **`TextareaInput`** pada form input berita.

    ```tsx
    <TextareaInput
      disabled={isFormDisabled}
      id="deskripsi"
      label="Deskripsi Berita"
      className={classNameForm}
      hasError={!!errors.description}
      placeholder="Masukan Deskripsi Berita"
      additionalClassNameLabel={additionalClassNameLabel}
      {...register("description")}
      renderErrorMessage={renderError("description")}
    />
    ```

    - Hal yang sama seperti input form **`judul`**. Tetapi memakai component **`TextareaInput`** dari **`@sekjen-dpr/react`**.

- Button Submit Form :

  ```tsx
  <div className="d-flex justify-content-between">
    <Button
      type="submit"
      disabled={isFormDisabled}
      isLoading={isFormLoading}
      color="primary"
    >
      Simpan
    </Button>
  </div>
  ```

- Setelah itu kita akan membuat **`index.ts`** didalam **`berita/src/components/Forms/BeritaForm`**

  ```ts
  export { default } from "./BeritaForm";
  ```

## Membuat Page Tambah Berita

- Kita akan membuat page tambah berita pada file **`berita/src/pages/Berita/BeritaAdd.tsx`**

  ```tsx
  import React from "react";
  import BeritaForm from "@/components/Forms/BeritaForm";

  const BeritaAdd = () => {
    return <BeritaForm />;
  };

  export default BeritaAdd;
  ```

  - **`import BeritaForm from "@/components/Forms/BeritaForm";`** : Mengimport komponen **`BeritaForm`** yang sudah kita buat sebelumnya.
  - **`return <BeritaForm />;`** : Mengembalikan komponen **`BeritaForm`**.

- Menambahkan Page Tambah Berita ke Route di **`berita/src/App.tsx`**

  ```diff
  ...
  + import BeritaAdd from "./pages/Berita/BeritaAdd";

  const App: React.FC<AppProps> = ({ history }) => {
   return (
     <HistoryRouter history={history}>
       <Routes>
         <Route path="/berita" element={<Layout />}>
           <Route index element={<Berita />} />
  +        <Route path="tambah" element={<BeritaAdd />} />
         </Route>
       </Routes>
     </HistoryRouter>
   );
  };

  export default App;
  ```

## Membuat Page Ubah Berita

- Kita akan membuat page tambah berita pada file **`berita/src/pages/Berita/BeritaEdit.tsx`**

  ```tsx
  import React from "react";
  import BeritaForm from "@/components/Forms/BeritaForm";
  import { useParams } from "react-router-dom";

  const BeritaEdit = () => {
    const { id } = useParams();
    return <BeritaForm id={id} />;
  };

  export default BeritaEdit;
  ```

  - **`import BeritaForm from "@/components/Forms/BeritaForm";`** : Mengimport komponen **`BeritaForm`** yang sudah kita buat sebelumnya.
  - **`import { useParams } from "react-router-dom";`** : digunakan untuk mengambil parameter dari url.
  - **`const { id } = useParams();`** : digunakan untuk mengambil parameter **`id`** dari url.
  - **`return <BeritaForm id={id} />;`** : mengembalikan komponen **`BeritaForm`** dengan mengirimkan parameter **`id`**.

- Menambahkan Page Ubah Berita ke Route di **`berita/src/App.tsx`**

  ```diff
  ...
  import BeritaAdd from "./pages/Berita/BeritaAdd";
  + import BeritaEdit from "./pages/Berita/BeritaEdit";

  const App: React.FC<AppProps> = ({ history }) => {
   return (
     <HistoryRouter history={history}>
       <Routes>
         <Route path="/berita" element={<Layout />}>
           <Route index element={<Berita />} />
           <Route path="tambah" element={<BeritaAdd />} />
  +        <Route path="ubah/:id" element={<BeritaEdit />} />
         </Route>
       </Routes>
     </HistoryRouter>
   );
  };

  export default App;
  ```

## Membuat Hapus Berita

- Menambahkan hook mutation untuk **`deleteBerita`** dari service Berita

  ```diff
  import {
    useCreateBeritaMutation,
  + useDeleteBeritaMutation,
    useGetBeritaByIdQuery,
    useUpdateBeritaMutation,
  } from "@/state/berita/service";
  ...
  const [createBerita] = useCreateBeritaMutation();
  const [updateBerita] = useUpdateBeritaMutation();
  + const [deleteBerita] = useDeleteBeritaMutation();
  ...
  + const onDelete = async () => {
  +  if (isEditMode) {
  +    await deleteBerita(id).unwrap();
  +    navigate("/berita");
  +  }
  + };
  ```

  - **`const [deleteBerita] = useDeleteBeritaMutation();`** : digunakan untuk memanggil hook **`useDeleteBeritaMutation`** yang akan digunakan untuk menghapus data berita.
  - **`await deleteBerita(id).unwrap();`** : digunakan untuk menghapus data berita berdasarkan id yang dikirimkan.

- Menambahkan button hapus pada page **`berita/src/pages/Berita/BeritaDetail.tsx`**.

  ```tsx
  <div className="d-flex justify-content-between">
    ...
    {isEditMode && (
      <Button
        type="button"
        disabled={isFormDisabled}
        isLoading={isFormLoading}
        color="danger"
        onClick={() => onDelete()}
      >
        Delete
      </Button>
    )}
  </div>
  ```

## Hasil Akhir Dari file-file yang sudah kita buat

### **`berita/src/components/Forms/BeritaForm.tsx`**

```tsx
import React, { useEffect } from "react";
import { BeritaFormField, BeritaItem } from "@/types/Berita";
import {
  useCreateBeritaMutation,
  useDeleteBeritaMutation,
  useGetBeritaByIdQuery,
  useUpdateBeritaMutation,
} from "@/state/berita/service";
import * as yup from "yup";
import {
  Card,
  TextInput,
  TextareaInput,
  useCustomHookForm,
  Button,
} from "@sekjen-dpr/react";
import { useNavigate } from "react-router-dom";

const schemaForm: yup.SchemaOf<BeritaFormField> = yup.object({
  title: yup.string().required("Judul tidak boleh kosong"),
  description: yup.string().required("Deskripsi tidak boleh kosong"),
});

const resetValue: BeritaFormField = {
  title: "",
  description: "",
};

interface BeritaFormProps {
  id?: string;
}

const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {
  const isEditMode = !!id;
  const navigate = useNavigate();
  const { data, isSuccess, isLoading, isFetching } = useGetBeritaByIdQuery(
    id ?? "",
    { skip: !isEditMode }
  );
  const [createBerita] = useCreateBeritaMutation();
  const [updateBerita] = useUpdateBeritaMutation();
  const [deleteBerita] = useDeleteBeritaMutation();
  const {
    isFormLoading,
    classNames: { additionalClassNameLabel, classNameForm },
    renderError,
    isFormDisabled,
    shouldUpdateDefaultValueWithData,
    setDefaultValue,
    useForm: { register, handleSubmit, errors, getValues },
  } = useCustomHookForm<BeritaFormField, BeritaItem>({
    schemaForm,
    isFetching,
    isLoading,
    isSuccess,
    data,
    isEditMode,
    resetValue,
  });
  useEffect(() => {
    if (shouldUpdateDefaultValueWithData && data) {
      setDefaultValue({ ...data });
    }
  }, [shouldUpdateDefaultValueWithData]);
  const onSubmit = handleSubmit(async () => {
    const values = getValues();
    if (isEditMode) {
      await updateBerita({ ...values, id: id ?? "" }).unwrap();
    } else {
      await createBerita(values).unwrap();
    }
    navigate("/berita");
  });
  const onDelete = async () => {
    if (isEditMode) {
      await deleteBerita(id).unwrap();
      navigate("/berita");
    }
  };

  return (
    <>
      <Card shadow className="mb-10 w-800px mx-auto mw-100">
        <Card.Header>
          <h3 className="card-title d-flex align-items-start flex-column">
            <span className="card-label fw-bold fs-3 mb-1">
              {isEditMode ? "Ubah" : "Tambah"} Berita
            </span>
          </h3>
        </Card.Header>
        <Card.Body>
          <form onSubmit={onSubmit}>
            <TextInput
              disabled={isFormDisabled}
              id="judul"
              label="Judul Berita"
              className={classNameForm}
              hasError={!!errors.title}
              placeholder="Masukan Judul Berita"
              additionalClassNameLabel={additionalClassNameLabel}
              {...register("title")}
              renderErrorMessage={renderError("title")}
            />
            <TextareaInput
              disabled={isFormDisabled}
              id="deskripsi"
              label="Deskripsi Berita"
              className={classNameForm}
              hasError={!!errors.description}
              placeholder="Masukan Deskripsi Berita"
              additionalClassNameLabel={additionalClassNameLabel}
              {...register("description")}
              renderErrorMessage={renderError("description")}
            />
            <div className="d-flex justify-content-between">
              <Button
                type="submit"
                disabled={isFormDisabled}
                isLoading={isFormLoading}
                color="primary"
              >
                Simpan
              </Button>
              {isEditMode && (
                <Button
                  type="button"
                  disabled={isFormDisabled}
                  isLoading={isFormLoading}
                  color="danger"
                  onClick={() => onDelete()}
                >
                  Delete
                </Button>
              )}
            </div>
          </form>
        </Card.Body>
      </Card>
    </>
  );
};

export default BeritaForm;
```

### **`berita/src/components/Forms/BeritaForm/index.ts`**

```tsx
export { default } from "./BeritaForm";
```

### **`berita/src/pages/Berita/BeritaAdd.tsx`**

```tsx
import React from "react";
import BeritaForm from "@/components/Forms/BeritaForm";

const BeritaAdd = () => {
  return <BeritaForm />;
};

export default BeritaAdd;
```

### **`berita/src/pages/Berita/BeritaEdit.tsx`**

```tsx
import React from "react";
import BeritaForm from "@/components/Forms/BeritaForm";
import { useParams } from "react-router-dom";

const BeritaEdit = () => {
  const { id } = useParams();
  return <BeritaForm id={id} />;
};

export default BeritaEdit;
```

### **`berita/src/App.tsx`**

```tsx
import React from "react";
import {
  Route,
  Routes,
  unstable_HistoryRouter as HistoryRouter,
} from "react-router-dom";
import { BrowserHistory } from "history";
import Layout from "@/components/Layout";
import Berita from "./pages/Berita";
import BeritaAdd from "./pages/Berita/BeritaAdd";
import BeritaEdit from "./pages/Berita/BeritaEdit";

interface AppProps {
  history: BrowserHistory;
}

const App: React.FC<AppProps> = ({ history }) => {
  return (
    <HistoryRouter history={history}>
      <Routes>
        <Route path="/berita" element={<Layout />}>
          <Route index element={<Berita />} />
          <Route path="tambah" element={<BeritaAdd />} />
          <Route path="ubah/:id" element={<BeritaEdit />} />
        </Route>
      </Routes>
    </HistoryRouter>
  );
};

export default App;
```
# rtk-query tags

rtk-query tags adalah cara untuk memanipulasi dan mengatur query dan mutation pada library rtk-query. Dengan menggunakan tags, kita dapat mengelompokkan beberapa query atau mutation, memberikan nama atau label pada query atau mutation tertentu, dan juga dapat memanipulasi dan mengatur cache secara spesifik pada query atau mutation tersebut.

Dalam rtk-query, tags diwakili oleh objek yang memiliki nama atau label, dan dapat ditambahkan ke argumen **`query`** atau **`mutation`** pada penggunaannya. Misalnya, kita dapat menambahkan tags pada penggunaan **`useQuery`** atau **`useMutation`**, seperti ini:

```js
const { data, isFetching } = useQuery("getBerita", getBerita, {
  tags: ["Berita"],
});
```

Dalam contoh di atas, tag **`"Berita"`** ditambahkan pada penggunaan useQuery dengan nama query **`"getBerita"`**. Dengan adanya tags, kita dapat menvalidasi ulang query ketika tag **`"Berita"`** di panggil oleh query/mutation lain.

## Menggunakan Tags pada service berita

- Pertama daftarkan tag **`"Berita"`** pada service berita:

  ```diff
  export const brBeritaService = createApi({
    reducerPath: "brBeritaService",
    baseQuery: baseQueryWithReauth,
  + tagTypes: ["Berita"],
  ...
  });
  ```

- Kemudian tambahkan tag **`"Berita"`** pada query **`getDaftarBerita`** pada service berita:

  ```diff
  getDaftarBerita: builder.query<Response<BeritaItem[]>, GetBeritaParameters>({
    query: ...
  + providesTags: (result) =>
  +     result
  +       ? [...result.data.map(({ id }) => ({ type: "Berita" as const, id })), { type: "Berita", id: "LIST" }]
  +       : [{ type: "Berita", id: "LIST" }],
  ...
  })
  ```

  - **`providesTags: (result) =>
result
  ? [...result.data.map(({ id }) => ({ type: "Berita" as const, id })), { type: "Berita", id: "LIST" }]
  : [{ type: "Berita", id: "LIST" }],`** : Tag **`"Berita"`** akan di berikan pada setiap item berita yang di dapatkan dari query **`getDaftarBerita`**. Tag **`"Berita"`** juga akan diberikan pada tag **`"LIST"`** untuk menandakan bahwa query **`getDaftarBerita`** telah di panggil.

- Kemudian tambahkan tag **`"Berita"`** pada query **`getBeritaById`** pada service berita:

  ```diff
  getBeritaById: builder.query<Response<BeritaItem>, string>({
    query: ...
  + providesTags: (_result, _error, id) => [{ type: "Berita", id }]
    ...
  })
  ```

  - **`providesTags: (_result, _error, id) => [{ type: "Berita", id }]`** : Tag **`"Berita"`** akan diberikan pada tag **`"{ type: "Berita", id }"`** untuk menandakan bahwa query **`getBeritaById`** telah di panggil.

- Kemudian tambahkan tag **`"Berita"`** pada query **`createBerita`** pada service berita:

  ```diff
  createBerita: builder.mutation<Response<BeritaItem>, FormData>({
    query: ...
  + invalidatesTags: [{ type: "Berita", id: "LIST" }],
    ...
  })
  ```

  - **`invalidatesTags: [{ type: "Berita", id: "LIST" }]`** : Tag **`"Berita"`** akan di invalidasi pada tag **`"{ type: "Berita", id: "LIST" }"`** untuk menandakan bahwa query **`getDaftarBerita`** perlu di panggil ulang.

- Kemudian tambahkan tag **`"Berita"`** pada query **`updateBerita`** pada service berita:

  ```diff
  updateBerita: builder.mutation<Response<BeritaItem>, BeritaFormField & { id: string | number }>({
    query: ...
  + invalidatesTags: (_result, _error, arg) => [{ type: "Berita", id: arg.id }],
    ...
  })
  ```

  - **`invalidatesTags: (_result, _error, arg) => [{ type: "Berita", id: arg.id }]`** : Tag **`"Berita"`** akan di invalidasi pada tag **`"{ type: "Berita", id: arg.id }"`** untuk menandakan bahwa query **`getBeritaById`** perlu di panggil ulang.

- Kemudian tambahkan tag **`"Berita"`** pada query **`deleteBerita`** pada service berita:

  ```diff
  deleteBerita: builder.mutation<Response<BeritaItem>, string>({
    query: ...
  + invalidatesTags: [{ type: "Berita", id: "LIST" }],
    ...
  })
  ```

## Hasil Akhir Dari file **`berita/src/state/berita/service.ts`**

```ts
import { BeritaFormField, BeritaItem } from "@/types/Berita";
import { createApi } from "@reduxjs/toolkit/query/react";
import {
  queryMaker,
  GQLTableQueryMaker,
  Response,
  DefaultListParameters,
  GQLQueryMaker,
} from "@sekjen-dpr/react";
import baseQueryWithReauth from "../baseQueryWithReauth";

type GetBeritaParameters = DefaultListParameters;

export const brBeritaService = createApi({
  reducerPath: "brBeritaService",
  baseQuery: baseQueryWithReauth,
  tagTypes: ["Berita"],
  endpoints: (builder) => ({
    getDaftarBerita: builder.query<Response<BeritaItem[]>, GetBeritaParameters>(
      {
        query: ({ sort, search, ...params }) =>
          queryMaker({
            body: GQLTableQueryMaker({
              field: "news",
              query: `
            data{
              id
              title
              description
            }
            `,
              variables: [
                {
                  queryArgType: "QueryNewsWhereWhereConditions",
                  variableName: "where",
                  variableType: "where",
                  variableValueWhere: {
                    AND: search,
                  },
                },
                ...(sort
                  ? [
                      {
                        queryArgType: "[QueryNewsOrderByOrderByClause!]",
                        variableName: "orderBy",
                        variableValue: sort,
                      },
                    ]
                  : []),
              ],
              ...params,
            }),
          }),
        transformResponse: (response: {
          data: { news: Response<BeritaItem[]> };
        }) => response.data.news,
        providesTags: (result) =>
          result
            ? [
                ...result.data.map(({ id }) => ({
                  type: "Berita" as const,
                  id,
                })),
                { type: "Berita", id: "LIST" },
              ]
            : [{ type: "Berita", id: "LIST" }],
      }
    ),
    getBeritaById: builder.query<BeritaItem, string | number>({
      query: (id) =>
        queryMaker({
          body: GQLQueryMaker({
            field: "newsById",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                variableName: "id",
                variableValue: id,
                queryArgType: "ID!",
              },
            ],
          }),
        }),
      transformResponse: (response: { data: { newsById: BeritaItem } }) =>
        response.data.newsById,
      providesTags: (_result, _error, id) => [{ type: "Berita", id }],
    }),
    createBerita: builder.mutation<Response<BeritaItem>, BeritaFormField>({
      query: (params) =>
        queryMaker({
          body: GQLQueryMaker({
            queryType: "mutation",
            field: "createNews",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                queryArgType: "NewsInput!",
                variableName: "input",
                variableValue: params,
              },
            ],
          }),
        }),
      invalidatesTags: [{ type: "Berita", id: "LIST" }],
    }),
    updateBerita: builder.mutation<
      Response<BeritaItem>,
      BeritaFormField & { id: string | number }
    >({
      query: ({ id, ...params }) =>
        queryMaker({
          body: GQLQueryMaker({
            queryType: "mutation",
            field: "updateNews",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                variableName: "id",
                variableValue: id,
                queryArgType: "ID!",
              },
              {
                queryArgType: "NewsInput!",
                variableName: "input",
                variableValue: params,
              },
            ],
          }),
        }),
      invalidatesTags: (_result, _error, arg) => [
        { type: "Berita", id: arg.id },
      ],
    }),
    deleteBerita: builder.mutation<Response<BeritaItem>, string | number>({
      query: (beritaId) =>
        queryMaker({
          body: GQLQueryMaker({
            queryType: "mutation",
            field: "deleteNews",
            query: `
              id
              title
              description
            `,
            variables: [
              {
                queryArgType: "ID!",
                variableName: "id",
                variableValue: beritaId,
              },
            ],
          }),
        }),
      invalidatesTags: [{ type: "Berita", id: "LIST" }],
    }),
  }),
});
export const {
  useGetDaftarBeritaQuery,
  useGetBeritaByIdQuery,
  useCreateBeritaMutation,
  useUpdateBeritaMutation,
  useDeleteBeritaMutation,
} = brBeritaService;
```

# Modal Konfirmasi

Kita akan membuat modal konfirmasi untuk mengapus, mengubah, dan menambahkan berita. Modal konfirmasi ini akan muncul ketika kita mengklik tombol hapus, ubah, dan tambah data.

- Kita akan membuat state **`isConfirmationModalOpen`** untuk menentukan apakah modal konfirmasi untuk **`ubah`**, dan **`tambah data`**.

  ```diff
  + import React, { useEffect, useState } from "react";
  const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {
    ...
  + const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
    ...
  }
  ```

  - **`isConfirmationModalOpen`** : State untuk menentukan apakah modal konfirmasi untuk **`ubah`**, dan **`tambah data`** terbuka atau tidak.

- Lalu kita akan membuat Konfirmasi modal dengan menggunakan component **`ConfirmationModal`** dari **`@sekjen-dpr/react`** pada file **`berita/src/Forms/BeritaForm/BeritaForm.tsx`**

  ```tsx
  ...
  import React, { useEffect, useState } from "react";
  import { Card, TextInput, TextareaInput, useCustomHookForm, Button, ConfirmationModal } from "@sekjen-dpr/react";

  const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {
    ...
  const onSubmit = handleSubmit(async () => {
    setIsConfirmationModalOpen(true);
  });
  const onConfirm = async () => {
    const values = getValues();
    if (isEditMode) {
      await updateBerita({ ...values, id: id ?? "" }).unwrap();
    } else {
      await createBerita(values).unwrap();
    }
  };
  ...
  ```

  dan tambahkan **`ConfirmationModal`** pada return

  ```tsx
  ...
  {isConfirmationModalOpen && (
    <ConfirmationModal
      message={
        isEditMode
          ? "Apakah Anda yakin ingin menyimpan perubahan ?"
          : "Apakah Anda yakin ingin menyimpan data ini ?"
      }
      confirmText={isEditMode ? "Simpan Perubahan" : "Simpan"}
      cancelText={isEditMode ? "Buang Perubahan" : "Batal"}
      successMessage={isEditMode ? "Perubahan berhasil disimpan" : "Data berhasil disimpan"}
      onConfirmAsync={onConfirm}
      onSuccessButtonClicked={() => {
        if (!isEditMode) {
          navigate("/berita");
        }
      }}
      onConfirmationModalClosed={() => {
        setIsConfirmationModalOpen(false);
      }}
    />
  )}
  ```

- Lalu kita akan membuat konfimasi modal untuk menghapus data.

  ```tsx
  ...

  const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {
  ...
  const [isDeleteConfirmationModalOpen, setIsDeleteConfirmationModalOpen] = useState(false);
  ...
  const onConfirmDelete = async () => {
    if (isEditMode) {
      await deleteBerita(id ?? "").unwrap();
    }
  };
  const onDelete = async () => {
    setIsDeleteConfirmationModalOpen(true);
  };
  ...
  ```

  dan tambahkan **`ConfirmationModal`** untuk hapus data pada return

  ```tsx
  ...
  {isDeleteConfirmationModalOpen && (
    <ConfirmationModal
      message="Apakah Anda yakin ingin menghapus data ini ?"
      confirmText="Hapus"
      cancelText="Batal"
      successMessage="Data berhasil dihapus"
      onConfirmAsync={onConfirmDelete}
      onSuccessButtonClicked={() => {
        navigate("/berita");
      }}
      onConfirmationModalClosed={() => {
        setIsDeleteConfirmationModalOpen(false);
      }}
    />
  )}
  ...
  ```

## Hasil Akhir Dari file **`berita/src/Forms/BeritaForm/BeritaForm.tsx`**

```tsx
import React, { useEffect, useState } from "react";
import { BeritaFormField, BeritaItem } from "@/types/Berita";
import {
  useCreateBeritaMutation,
  useDeleteBeritaMutation,
  useGetBeritaByIdQuery,
  useUpdateBeritaMutation,
} from "@/state/berita/service";
import * as yup from "yup";
import {
  Card,
  TextInput,
  TextareaInput,
  useCustomHookForm,
  Button,
  ConfirmationModal,
} from "@sekjen-dpr/react";
import { useNavigate } from "react-router-dom";

const schemaForm: yup.SchemaOf<BeritaFormField> = yup.object({
  title: yup.string().required("Judul tidak boleh kosong"),
  description: yup.string().required("Deskripsi tidak boleh kosong"),
});

const resetValue: BeritaFormField = {
  title: "",
  description: "",
};

interface BeritaFormProps {
  id?: string;
}

const BeritaForm: React.FC<BeritaFormProps> = ({ id }) => {
  const isEditMode = !!id;
  const navigate = useNavigate();
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
  const [isDeleteConfirmationModalOpen, setIsDeleteConfirmationModalOpen] =
    useState(false);
  const { data, isSuccess, isLoading, isFetching } = useGetBeritaByIdQuery(
    id ?? "",
    { skip: !isEditMode }
  );
  const [createBerita] = useCreateBeritaMutation();
  const [updateBerita] = useUpdateBeritaMutation();
  const [deleteBerita] = useDeleteBeritaMutation();
  const {
    isFormLoading,
    classNames: { additionalClassNameLabel, classNameForm },
    renderError,
    isFormDisabled,
    shouldUpdateDefaultValueWithData,
    setDefaultValue,
    useForm: { register, handleSubmit, errors, getValues },
  } = useCustomHookForm<BeritaFormField, BeritaItem>({
    schemaForm,
    isFetching,
    isLoading,
    isSuccess,
    data,
    isEditMode,
    resetValue,
  });
  useEffect(() => {
    if (shouldUpdateDefaultValueWithData && data) {
      setDefaultValue({ ...data });
    }
  }, [shouldUpdateDefaultValueWithData]);
  const onSubmit = handleSubmit(async () => {
    setIsConfirmationModalOpen(true);
  });
  const onConfirm = async () => {
    const values = getValues();
    if (isEditMode) {
      await updateBerita({ ...values, id: id ?? "" }).unwrap();
    } else {
      await createBerita(values).unwrap();
    }
  };
  const onConfirmDelete = async () => {
    if (isEditMode) {
      await deleteBerita(id ?? "").unwrap();
    }
  };
  const onDelete = async () => {
    setIsDeleteConfirmationModalOpen(true);
  };

  return (
    <>
      <Card shadow className="mb-10 w-800px mx-auto mw-100">
        <Card.Header>
          <h3 className="card-title d-flex align-items-start flex-column">
            <span className="card-label fw-bold fs-3 mb-1">
              {isEditMode ? "Ubah" : "Tambah"} Berita
            </span>
          </h3>
        </Card.Header>
        <Card.Body>
          <form onSubmit={onSubmit}>
            <TextInput
              disabled={isFormDisabled}
              id="judul"
              label="Judul Berita"
              className={classNameForm}
              hasError={!!errors.title}
              placeholder="Masukan Judul Berita"
              additionalClassNameLabel={additionalClassNameLabel}
              {...register("title")}
              renderErrorMessage={renderError("title")}
            />
            <TextareaInput
              disabled={isFormDisabled}
              id="deskripsi"
              label="Deskripsi Berita"
              className={classNameForm}
              hasError={!!errors.description}
              placeholder="Masukan Deskripsi Berita"
              additionalClassNameLabel={additionalClassNameLabel}
              {...register("description")}
              renderErrorMessage={renderError("description")}
            />
            <div className="d-flex justify-content-between">
              <Button
                type="submit"
                disabled={isFormDisabled}
                isLoading={isFormLoading}
                color="primary"
              >
                Simpan
              </Button>
              {isEditMode && (
                <Button
                  type="button"
                  disabled={isFormDisabled}
                  isLoading={isFormLoading}
                  color="danger"
                  onClick={() => onDelete()}
                >
                  Delete
                </Button>
              )}
            </div>
          </form>
        </Card.Body>
      </Card>
      {isConfirmationModalOpen && (
        <ConfirmationModal
          message={
            isEditMode
              ? "Apakah Anda yakin ingin menyimpan perubahan ?"
              : "Apakah Anda yakin ingin menyimpan data ini ?"
          }
          confirmText={isEditMode ? "Simpan Perubahan" : "Simpan"}
          cancelText={isEditMode ? "Buang Perubahan" : "Batal"}
          successMessage={
            isEditMode
              ? "Perubahan berhasil disimpan"
              : "Data berhasil disimpan"
          }
          onConfirmAsync={onConfirm}
          onSuccessButtonClicked={() => {
            if (!isEditMode) {
              navigate("/berita");
            }
          }}
          onConfirmationModalClosed={() => {
            setIsConfirmationModalOpen(false);
          }}
        />
      )}
      {isDeleteConfirmationModalOpen && (
        <ConfirmationModal
          message="Apakah Anda yakin ingin menghapus data ini ?"
          confirmText="Hapus"
          cancelText="Batal"
          successMessage="Data berhasil dihapus"
          onConfirmAsync={onConfirmDelete}
          onSuccessButtonClicked={() => {
            navigate("/berita");
          }}
          onConfirmationModalClosed={() => {
            setIsDeleteConfirmationModalOpen(false);
          }}
        />
      )}
    </>
  );
};

export default BeritaForm;
```
# Deployment

Untuk proses **deployment** kita menggunakan fitur **CI/CD** dari **gitlab** yang dimana proses dilakukan secara otomatis ketika **branch** tertentu ter-update.

- branch **develop** untuk deployment versi **staging**
- branch **main** untuk deployment versi **production**

Berikut adalah **flowchart** dari proses deployment:

![deployment flow](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-13.png)

- Terdapat repository yang menjadi repository utama untuk bertugas sebagai mengumpulkan semua **repository** yang ada pada aplikasi ini, yaitu repository [main](https://gitlab.com/sekjen-dpr/microfrontend/main/). Didalam Repository ini terdapat **submodule** yang berisi **repository** dari setiap module yang ada pada aplikasi ini.
- Dari **flowchart** diatas kita bisa melihat bahwa proses deployment akan dijalankan ketika terjadi perubahan pada **branch** **develop** atau **main** dari setiap submodule didalam repository utama([main](https://gitlab.com/sekjen-dpr/microfrontend/main/)).
- Setelah ada **push** atau perubahan pada **branch** **develop** atau **main** dari setiap submodule, maka akan terjadi **trigger** pada **pipeline(Sub Module CI/CD)**, dari proses tersebut menghasilkan **artifact** yang akan diupload ke **`gitlab storage`**.
- Setelah **artifact** diupload ke **`gitlab storage`** maka akan terjadi **trigger** pada **pipeline(Main Module CI/CD)**, dimana pada proses tersebut **pipeline** akan mengambil semua artifact-artifact semua module di aplikasi ini dari **`gitlab storage`** dan menjadikannya 1 web aplikasi yang siap di deploy ke **production** atau **staging**.

## Deployment di Real Environment

Misalnya kita akan membuat module baru yaitu **`Berita`**. Berikut adalah langkah-langkah yang harus kita lakukan:

### 1. Membuat repository Berita

Kita harus membuat repository baru dengan nama **`Berita`** di group project [**`sekjen-dpr/microfrontend`**](https://gitlab.com/sekjen-dpr/microfrontend), dan jangan lupa untuk mengatur environment variable agar dapat digunakan pada proses CI/CD.

- Pertama kita harus membuat environment untuk repository dengan cara masuk ke menu **Deployments** lalu ke **Environments**

  ![Environment](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-14.png)
  lalu klik **New Environment**

- Lalu buat environment dengan nama **`staging`** dan **`production`**.

  ![Environment](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-15.png)

- Setelah itu kita harus membuat **variable** untuk environment **`staging`** dan **`production`** dengan cara ke menu **Settings** > **CI/CD** > **Variables**.

  ![Environment](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-16.png)
  lalu klik **Add Variable**

- Lalu buat variable yang sesuai dengan **environmentnya (staging atau production)**.

  ![Environment](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-17.jpg)

### 2. Konfigurasi Gitlab CI/CD pada Sub Module

Didalam source module **`Berita`** kita dapat menemukan file **`.gitlab-ci.yml.example`**, itu adalah file konfigurasi untuk gitlab CI/CD. Kita hanya perlu mengubah nama nya menjadi **`.gitlab-ci.yml`**.

### 3. Konfigurasi pada Main repository

Di Main repository kita harus menambahkan **submodule Berita** dengan cara:

```bash
git submodule add https://gitlab.com/sekjen-dpr/microfrontend/berita.git
```

Setelah itu kita harus mendaftarkan konfigurasi artifact di file **`.gitlab-ci.yml`** pada main repository dengan cara:

```diff
build-staging:
  stage: build
  environment:
    name: staging
  only:
    - develop
  before_script:
    - apk add --update curl unzip && rm -rf /var/cache/apk/*
  script:
    ...
+   - 'curl --location --output artifact-berita.zip --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/{PROJECT ID GITLAB}/jobs/artifacts/develop/download?job=build-staging"'
    - ls -a
    - mkdir build && mkdir build/static
    - cp Dockerfile build/Dockerfile
    - printf "server {\n listen 80;\n server_name frontend;\n location / {\n root /usr/share/nginx/html;\n index  index.html index.htm;\n try_files \$uri /index.html\$is_args\$args =404;\n }\n error_page   500 502 503 504  /50x.html;\n location = /50x.html {\n root   /usr/share/nginx/html;\n }\n location /api/graphql {\n proxy_pass $API_URL;\n }\n gzip_static on; \n}" >> build/nginx.conf
    ...
+   - mkdir artifact-berita && mkdir build/static/berita && unzip artifact-berita.zip -d artifact-berita && mv artifact-berita/dist/* build/static/berita
```

- Bisa dilihat diatas untuk {PROJECT ID GITLAB} harus diganti dengan **project id** dari repository.

  ![Environment](https://gitlab.com/mochammadkhasin/image-materi-microfrontend/-/raw/main/images/materi-18.png)
